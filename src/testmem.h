/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef TESTMEM_H
#define TESTMEM_H


#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QTimer>

#include <QDebug>

class TestMem : public QGroupBox
{
    Q_OBJECT

public:
    TestMem();
    ~TestMem();

public slots:
    void memCheck();

signals:
    void log(QString logString);


private:
    QLabel *infoLabel;
    QVBoxLayout *layout;
    QTimer *testTimer;



    long operations;

    int byteCounter;

    int numBuffer;
    int oldBuffer;
    unsigned char *buffer[2];
    unsigned char *bufferPosition;
    unsigned long allocSize;

    qint64 errorCount;
};


#endif // TESTMEM_H
