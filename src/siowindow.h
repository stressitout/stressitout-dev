/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef SIOWINDOW_H
#define SIOWINDOW_H


#include <QApplication>
#include <QMainWindow>
#include <QSettings>
#include <QString>
#include <QWidget>
#include <QTabWidget>
#include <QMenuBar>
#include <QStatusBar>
#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
//#include <QFormLayout>
#include <QSplitter>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QProgressBar>
#include <QTableWidget>
#include <QTimer>
#include <QCloseEvent>
#include <QMessageBox>
#include <QDir>
#include <QDesktopServices>
#include <QFileDialog>
#include <QUrl>

#include "testcpu.h"
#include "testmem.h"
#include "test2d.h"
#include "test3d.h"
#include "testdisk.h"
#include "testoptical.h"
#include "testserial.h"
#include "machineinfotable.h"
#include "logger.h"


class SIOWindow : public QMainWindow
{
    Q_OBJECT


public:
    SIOWindow();
    ~SIOWindow();

    void closeEvent(QCloseEvent *event);

    void setBaseTitleBarText(QString);
    void showFirstTimeWarning();
    void createMenus();

public slots:
    void startTests();
    void stopTests();
    void updateElapsedTime();
    void updateSensorsInfo();

    void saveLog();

    void toggleFullscreen(bool state);
    void toggleFlatButtons(bool state);

    void openWebsite();
    void aboutQt();
    void aboutFS();
    void aboutSIO();


private:
    QString baseTitleBarText;
    bool firstRun;
    bool testsRunning;

    QMenu *fileMenu;
    QMenu *viewMenu;
    QMenu *testsMenu;
    QMenu *helpMenu;

    QAction *saveLogAction;
    QAction *quitAction;

    QAction *viewFullscreenAction;
    QAction *viewFlatButtonsAction;

    QAction *startAction;
    QAction *stopAction;

    QAction *visitWebAction;
    QAction *aboutQtAction;
    QAction *aboutFreeSoftwareAction;
    QAction *aboutAction;



    // main widget and sub-widgets for the 4 tabs
    QTabWidget *centralWidget;

    QWidget  *runTabWidget;
    QWidget *runTabTopWidget;
    QWidget *runTabMiddleWidget;

    Logger *logTabWidget;

    QTextEdit *sensorsTabWidget;
    QTimer *sensorsInfoTimer;

    QWidget *confTabWidget;



    // Layouts
    QSplitter *centralSplitter;
    QVBoxLayout *centralLayout;

    QHBoxLayout *topLayout;
    QVBoxLayout *topRightLayout;
    QHBoxLayout *middleLayout;
    QHBoxLayout *bottomLayout;
    QHBoxLayout *bottomLeftLayout;

    QGridLayout *confTabLayout;



    // Widgets for the "Run" tab
    MachineInfoTable *machineInfoTable;
    QLabel *elapsedTimeLabel;
    QTimer *elapsedTimeTimer;
    int elapsedSec;
    int elapsedMin;
    int elapsedHour;
    QLabel *startingTimeLabel;

    QLabel *tmpLabel;

    QLabel *testPlaceholderLabel;

    QLabel *testDurationLabel;
    QSpinBox *testDurationHoursBox;
    QSpinBox *testDurationMinBox;
    QPushButton *startTestButton;
    QPushButton *stopTestButton;
    QPushButton *quitButton;



    // Widgets for the "Configuration" tab
    QCheckBox *cpuCheckBox;
    QSpinBox  *cpuThreadsConfig;
    QCheckBox *memCheckBox;
    QCheckBox *g2dCheckBox;
    QCheckBox *g3dCheckBox;
    QCheckBox *diskCheckBox;
    QCheckBox *opticCheckBox;
    QCheckBox *serialCheckBox;
    QLineEdit *serialConfig;
    QCheckBox *parallelCheckBox;
    QCheckBox *networkCheckBox;


    // Custom widgets for different test modules
    TestCPU     *cpuWidget;
    TestMem     *memWidget;
    Test2d      *g2dWidget;
    Test3d      *g3dWidget;
    TestDisk    *diskWidget;
    TestOptical *opticalWidget;
    TestSerial  *serialWidget;


};



#endif // SIOWINDOW_H
