<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>MachineInfoTable</name>
    <message>
        <location filename="../src/machineinfotable.cpp" line="35"/>
        <source>Machine specifications</source>
        <translation>Características del equipo</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Disks</source>
        <translation>Discos</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CD/DVD</source>
        <translation>CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="55"/>
        <source>No optical drives found</source>
        <translation>No se han encontrado unidades ópticas</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="310"/>
        <source>%1  (MAC %2)</source>
        <translation>%1  (MAC %2)</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="413"/>
        <source>%1 days, %2 hours, %3 minutes, %4 seconds</source>
        <oldsource>%1 days %2 hours, %3 minutes, %4 seconds</oldsource>
        <translation>%1 días, %2 horas, %3 minutos, %4 segundos</translation>
    </message>
</context>
<context>
    <name>SIOWindow</name>
    <message>
        <location filename="../src/siowindow.cpp" line="34"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="51"/>
        <source>&amp;Run Tests</source>
        <translation>&amp;Ejecutar tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="53"/>
        <source>S&amp;ensors</source>
        <translation>Se&amp;nsores</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="55"/>
        <source>&amp;Log</source>
        <translation>&amp;Registro</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="57"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="126"/>
        <source>Tests not started</source>
        <translation>Tests no iniciados</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="168"/>
        <source> minutes</source>
        <translation> minutos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="180"/>
        <source>&amp;Start tests</source>
        <translation>&amp;Iniciar tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="185"/>
        <source>&amp;Stop tests</source>
        <translation>&amp;Detener tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="194"/>
        <location filename="../src/siowindow.cpp" line="430"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="156"/>
        <source>R&amp;un for</source>
        <translation>E&amp;jecutar durante</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="160"/>
        <source> hours</source>
        <translation> horas</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="163"/>
        <source>Duration of the tests, hours</source>
        <translation>Duración de los tests, horas</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="171"/>
        <source>Duration of the tests, minutes</source>
        <translation>Duración de los tests, minutos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="208"/>
        <source>Preparing sensors information...</source>
        <translation>Preparando información de sensores...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="232"/>
        <source>Kernel: </source>
        <translation>Kernel: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="235"/>
        <source>System uptime: </source>
        <translation>Uptime del sistema: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="249"/>
        <source>Screen resolution: </source>
        <translation>Resolución de pantalla: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="250"/>
        <source>Ideal CPU thread count: %1</source>
        <translation>Número ideal de hilos de CPU: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="254"/>
        <source>libsensors version: %1</source>
        <translation>libsensors versión: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="269"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="270"/>
        <source>Generate a high processing load to stress the CPU.</source>
        <translation>Generar una gran carga de procesamiento para estresar la CPU.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="276"/>
        <source> threads</source>
        <translation> hilos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="280"/>
        <location filename="../src/siowindow.cpp" line="516"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="281"/>
        <source>Test RAM.</source>
        <translation>Comprobar la RAM.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="285"/>
        <location filename="../src/siowindow.cpp" line="523"/>
        <source>2D graphics</source>
        <translation>Gráficos 2D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="286"/>
        <source>OpenGL 2D graphics drawing test.</source>
        <translation>Test de dijado de gráficos 2D OpenGL.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="290"/>
        <location filename="../src/siowindow.cpp" line="530"/>
        <source>3D graphics</source>
        <translation>Gráficos 3D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="291"/>
        <source>OpenGL 3D graphics test.</source>
        <translation>Test de gráficos 3D OpenGL.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="295"/>
        <source>Hard Disk drives</source>
        <translation>Unidades de disco duro</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="296"/>
        <source>Test hard disk drives.</source>
        <translation>Comprobar unidades de disco duro.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="300"/>
        <location filename="../src/siowindow.cpp" line="544"/>
        <source>Optical drives</source>
        <translation>Unidades ópticas</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="301"/>
        <source>Test CD, DVD or Bluray drives.</source>
        <translation>Comprobar unidades de CD, DVD o Bluray.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="305"/>
        <location filename="../src/siowindow.cpp" line="551"/>
        <source>Serial ports</source>
        <translation>Puertos série</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="306"/>
        <source>Test transmission on serial (COM) ports. Requires a loopback plug.</source>
        <translation>Comprobar la transmisión en puertos serie (COM). Es necesario un conector loopback.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="313"/>
        <location filename="../src/siowindow.cpp" line="558"/>
        <source>Parallel ports</source>
        <translation>Puertos paralelos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="314"/>
        <source>Test transmission on parallel (LPT) ports. Requires a loopback plug.</source>
        <translation>Comprobar la transmisión en puertos paralelos (LTP). Es necesario un conector loopback.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="319"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="320"/>
        <source>Test Ethernet network connection</source>
        <translation>Comprobar la conexión de red Ethernet</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="328"/>
        <location filename="../src/siowindow.cpp" line="717"/>
        <source>Ready</source>
        <translation>Preparado</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="508"/>
        <source>CPU load</source>
        <translation>Carga de CPU</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="537"/>
        <source>Disks</source>
        <translation>Discos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="562"/>
        <source>Network ports</source>
        <translation>Puertos de red</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="853"/>
        <source>Information about the Qt Framework</source>
        <translation>Mostrar información de la versión de Qt Framework</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="876"/>
        <source>StressItOut provides several modules to allow testing and stressing some or all the components of the computer.&lt;br&gt;These tests are intended to be run on newly-built computers or in order to verify stability after some hardware changes, like overclocking.&lt;br&gt;&lt;br&gt;It is NOT recommended to run these tests on a production (regular/serious use) computer, they could damage it, resulting in data loss.&lt;br&gt;&lt;br&gt;Use carefully.</source>
        <translation>StressItOut proporciona diversos módulos que permiten probar y estresar algunos o todos los componentes del ordenador.&lt;br&gt;Estos tests están pensados para ser ejecutados en ordenadores recien montados, o para verificar la estabilidad después de algunos cambios al hardware, como &apos;overclocking&apos;.&lt;br&gt;NO se recomienda ejecutar estos tests en un ordenador de producción (uso regular/serio), podrían dañarlo, resultando en pérdida de datos.&lt;br&gt;Utilízalo con precaución.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="883"/>
        <source>Application icon based on one of Oxygen&apos;s icons: http://www.oxygen-icons.org/ (LGPL license)</source>
        <translation>Icono de la aplicación basado en uno de los iconos Oxygen: http://www.oxygen-icons.org/ (licencia LGPL)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="885"/>
        <source>English translation by JanKusanagi.</source>
        <comment>Translators: change this string with your corresponding language and name</comment>
        <translation>Traducción al español/castellano por JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="887"/>
        <source>Visit http://jancoding.wordpress.com/stressitout/ for more information.</source>
        <translation>Visita http://jancoding.wordpress.com/stressitout/ para más información.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="425"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="426"/>
        <source>&amp;Save log to file</source>
        <translation>&amp;Guardar registro en un archivo</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="438"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="439"/>
        <source>&amp;Full screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="452"/>
        <source>&amp;Tests</source>
        <translation>&amp;Tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="453"/>
        <source>&amp;Start</source>
        <translation>&amp;Iniciar</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="456"/>
        <source>&amp;Stop</source>
        <translation>&amp;Detener</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="466"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="467"/>
        <source>Visit &amp;website</source>
        <translation>Visitar sitio &amp;web</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="478"/>
        <source>What is Free Software?</source>
        <translation>¿Qué es el software libre?</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="610"/>
        <source>Tests started. Duration: %1h %2m.</source>
        <translation>Tests iniciados. Duración: %1h %2m.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="648"/>
        <source>Tests stopped</source>
        <translation>Tests detenidos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="712"/>
        <source>Tests stopped after %1h %2m %3s.</source>
        <translation>Los tests se han detenido después de %1h %2m %3s.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="756"/>
        <source>Tests ended after %n hour(s)</source>
        <translation>
            <numerusform>Los tests han terminado después de %n hora</numerusform>
            <numerusform>Los tests han terminado después de %n horas</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="758"/>
        <source>and %n minute(s).</source>
        <translation>
            <numerusform>y %n minuto.</numerusform>
            <numerusform>y %n minutos.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="473"/>
        <source>About &amp;Qt</source>
        <translation>Acerca de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="225"/>
        <source>%1 version %2 started with PID %3, on %4.</source>
        <translation>%1 versión %2 iniciado con PID %3, el %4.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="239"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="240"/>
        <source>Memory:</source>
        <translation>Memoria:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="241"/>
        <source>Hard disks:
</source>
        <translation>Discos duros:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="242"/>
        <source>Optical drives: %1</source>
        <translation>Unidades ópticas: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="244"/>
        <source>No optical drives found</source>
        <translation>No se han encontrado unidades ópticas</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="245"/>
        <source>Network interfaces:</source>
        <translation>Interfaces de red:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="409"/>
        <source>WARNING, MAY EXPLODE!</source>
        <translation>¡¡CUIDADO, PUEDE EXPLOTAR!!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="410"/>
        <source>Well, not really... ;)

But using this program might be dangerous for this computer.

It is not recommended to run these tests on a production machine, as they could damage it (if its condition is not good), resulting in data loss.</source>
        <translation>Bueno, en realidad no... ;)

Pero usar este programa podría ser peligroso para este ordenador.

No se recomienda ejecutar estos tests en una máquina de producción, ya que podrían dañarla (si no está en buenas condiciones), resultando en una pérdida de datos.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="444"/>
        <source>Flat &amp;buttons</source>
        <translation>&amp;Botones planos</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="474"/>
        <source>Show Qt&apos;s version information</source>
        <translation>Mostrar información de la versión de Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="500"/>
        <source>Wrong test duration</source>
        <translation>Duración de test no válida</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="501"/>
        <source>0 time!!</source>
        <translation>Tiempo 0!!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="576"/>
        <source>No tests selected in configuration. Tests not started.</source>
        <translation>No hay ningún test seleccionado en la configuración. Tests no iniciados.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="583"/>
        <source>Testing</source>
        <translation>Probando</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="605"/>
        <source>%1 (Running)</source>
        <translation>%1 (Ejecutando)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="606"/>
        <source>Tests started at %1 on %2</source>
        <translation>Tests iniciados a las %1 el %2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="622"/>
        <source>Running tests...</source>
        <translation>Ejecutando tests...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="755"/>
        <source>Tests finished</source>
        <translation>Tests finalizados</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="784"/>
        <source>Save log to a file</source>
        <translation>Guardar registro en un archivo</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="786"/>
        <source>Log files (*.log);; All files (*)</source>
        <translation>Archivos de registro (*.log);; Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="796"/>
        <source>Error saving log</source>
        <translation>Error guardando el registro</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="797"/>
        <source>There was an error saving the log to %1:

%2</source>
        <translation>Ha ocurrido un error al guardar el registro en %1:

%2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="871"/>
        <source>About StressItOut</source>
        <translation>Acerca de StressItOut</translation>
    </message>
</context>
<context>
    <name>SensorsManager</name>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="25"/>
        <source>You should see sensor values here.</source>
        <translation>Aquí deberían verse valores de los sensores.</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="70"/>
        <source>Chip %1</source>
        <translation>Chip %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="71"/>
        <source>Address: %1</source>
        <translation>Dirección: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="72"/>
        <source>Path: %1</source>
        <translation>Ruta: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="73"/>
        <source>Prefix: %1</source>
        <translation>Prefijo: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="77"/>
        <source>Features:</source>
        <translation>Características:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="94"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="148"/>
        <source>No sensors detected, or not properly configured.
See lm_sensors documentation for more information.

You&apos;ll probably need to run sensors-detect.</source>
        <translation>No se han detectado sensores, o no están bien configurados.
Mira la documentación de lm_sensors para más información.

Probablemente necesites ejecutar sensors-detect.</translation>
    </message>
</context>
<context>
    <name>Test2d</name>
    <message>
        <location filename="../src/test2d.cpp" line="26"/>
        <source>2D Test</source>
        <translation>Test 2D</translation>
    </message>
</context>
<context>
    <name>Test3d</name>
    <message>
        <location filename="../src/test3d.cpp" line="26"/>
        <source>3D Test</source>
        <translation>Test 3D</translation>
    </message>
</context>
<context>
    <name>TestCPU</name>
    <message>
        <location filename="../src/testcpu.cpp" line="26"/>
        <source>CPU load</source>
        <translation>Carga de CPU</translation>
    </message>
    <message>
        <location filename="../src/testcpu.cpp" line="92"/>
        <source>Threads: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operations: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Hilos: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operaciones: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestDisk</name>
    <message>
        <location filename="../src/testdisk.cpp" line="27"/>
        <source>Disk Test</source>
        <translation>Test de disco</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="52"/>
        <source>No disks detected.</source>
        <translation>No se han detectado discos.</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="99"/>
        <source>Read: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Drive: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Leído: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Unidad: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestMem</name>
    <message>
        <location filename="../src/testmem.cpp" line="26"/>
        <source>Memory Test</source>
        <translation>Test de memoria</translation>
    </message>
    <message>
        <location filename="../src/testmem.cpp" line="85"/>
        <source>Verified: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Region: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Verificado: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Región: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errores: &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestOptical</name>
    <message>
        <location filename="../src/testoptical.cpp" line="26"/>
        <source>CD/DVD Test</source>
        <translation>Test de CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="55"/>
        <source>No optical drives.</source>
        <translation>No hay unidades ópticas.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="103"/>
        <source>Optical: Reached end of disc. Restarting.</source>
        <translation>Óptico: Alcanzado el final del disco. Reiniciando.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="112"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Read: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Loops: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%4&lt;/b&gt;</source>
        <translation>Unidad: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Leído: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Vueltas: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errores: &lt;b&gt;%4&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="123"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Unidad: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestSerial</name>
    <message>
        <location filename="../src/testserial.cpp" line="28"/>
        <source>Serial Test</source>
        <translation>Test de puerto série</translation>
    </message>
    <message>
        <location filename="../src/testserial.cpp" line="98"/>
        <source>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ports:&lt;b&gt;</source>
        <translation>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Puertos:&lt;b&gt;</translation>
    </message>
</context>
</TS>
