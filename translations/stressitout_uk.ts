<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk">
<context>
    <name>MachineInfoTable</name>
    <message>
        <location filename="../src/machineinfotable.cpp" line="35"/>
        <source>Machine specifications</source>
        <translation>Специфікація машини</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CPU</source>
        <translation>Процесор</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Disks</source>
        <translation>Диски</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CD/DVD</source>
        <translation>CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Network</source>
        <translation>Мережа</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="55"/>
        <source>No optical drives found</source>
        <translation>Оптичних пристроїв не знайдено</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="310"/>
        <source>%1  (MAC %2)</source>
        <translation>%1  (MAC %2)</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="413"/>
        <source>%1 days, %2 hours, %3 minutes, %4 seconds</source>
        <translation>%1 д, %2 г, %3 х, %4 с</translation>
    </message>
</context>
<context>
    <name>SIOWindow</name>
    <message>
        <location filename="../src/siowindow.cpp" line="34"/>
        <source>Initializing...</source>
        <translation>Ініціалізація...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="51"/>
        <source>&amp;Run Tests</source>
        <translation>&amp;Запустити тести</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="53"/>
        <source>S&amp;ensors</source>
        <translation>С&amp;енсори</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="55"/>
        <source>&amp;Log</source>
        <translation>&amp;Звіт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="57"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Конфігурація</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="126"/>
        <source>Tests not started</source>
        <translation>Тест не запущено</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="156"/>
        <source>R&amp;un for</source>
        <translation>За&amp;пустити на</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="160"/>
        <source> hours</source>
        <translation> год.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="163"/>
        <source>Duration of the tests, hours</source>
        <translation>Тривалість тестів, годин</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="168"/>
        <source> minutes</source>
        <translation> хв.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="171"/>
        <source>Duration of the tests, minutes</source>
        <translation>Тривалість тестів, хвилин</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="180"/>
        <source>&amp;Start tests</source>
        <translation>&amp;Старт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="185"/>
        <source>&amp;Stop tests</source>
        <translation>&amp;Стоп</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="194"/>
        <location filename="../src/siowindow.cpp" line="430"/>
        <source>&amp;Quit</source>
        <translation>&amp;Вихід</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="208"/>
        <source>Preparing sensors information...</source>
        <translation>Підготовка інформації з сенсорів...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="225"/>
        <source>%1 version %2 started with PID %3, on %4.</source>
        <translation>%1 версія %2 запущена з PID %3 %4.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="232"/>
        <source>Kernel: </source>
        <translation>Ядро: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="235"/>
        <source>System uptime: </source>
        <translation>Система працює: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="239"/>
        <source>CPU:</source>
        <translation>Процесор:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="240"/>
        <source>Memory:</source>
        <translation>Пам&apos;ять:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="241"/>
        <source>Hard disks:
</source>
        <translation>Жорсткі диски:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="242"/>
        <source>Optical drives: %1</source>
        <translation>Оптичні приводи: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="244"/>
        <source>No optical drives found</source>
        <translation>Оптичних пристроїв не знайдено</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="245"/>
        <source>Network interfaces:</source>
        <translation>Мережеві інтерфейси:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="249"/>
        <source>Screen resolution: </source>
        <translation>Екран: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="250"/>
        <source>Ideal CPU thread count: %1</source>
        <translation>Кількість ідеальних потоків процесора: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="254"/>
        <source>libsensors version: %1</source>
        <translation>версія libsensors: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="269"/>
        <source>CPU</source>
        <translation>Процесор</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="270"/>
        <source>Generate a high processing load to stress the CPU.</source>
        <translation>Створеня високого навантаження для стресу процесора.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="276"/>
        <source> threads</source>
        <translation> потоків</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="280"/>
        <location filename="../src/siowindow.cpp" line="516"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="281"/>
        <source>Test RAM.</source>
        <translation>Тест пам&apos;яті.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="285"/>
        <location filename="../src/siowindow.cpp" line="523"/>
        <source>2D graphics</source>
        <translation>Графіка 2D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="286"/>
        <source>OpenGL 2D graphics drawing test.</source>
        <translation>Тест графіки OpenGL 2D. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="290"/>
        <location filename="../src/siowindow.cpp" line="530"/>
        <source>3D graphics</source>
        <translation>Графіка 3D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="291"/>
        <source>OpenGL 3D graphics test.</source>
        <translation>Тест графіки OpenGL 3D. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="295"/>
        <source>Hard Disk drives</source>
        <translation>Жорсткі диски</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="296"/>
        <source>Test hard disk drives.</source>
        <translation>Тест жорстких дисків.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="300"/>
        <location filename="../src/siowindow.cpp" line="544"/>
        <source>Optical drives</source>
        <translation>Оптичні приводи</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="301"/>
        <source>Test CD, DVD or Bluray drives.</source>
        <translation>Тест приводів CD, DVD чи Bluray. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="305"/>
        <location filename="../src/siowindow.cpp" line="551"/>
        <source>Serial ports</source>
        <translation>Послідовні порти</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="306"/>
        <source>Test transmission on serial (COM) ports. Requires a loopback plug.</source>
        <translation>Тест передачі в COM-порт. Потрібна заглушка loopback. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="313"/>
        <location filename="../src/siowindow.cpp" line="558"/>
        <source>Parallel ports</source>
        <translation>Паралельні порти</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="314"/>
        <source>Test transmission on parallel (LPT) ports. Requires a loopback plug.</source>
        <translation>Тест передачі в LPT-порт. Потрібна заглушка loopback. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="319"/>
        <source>Network</source>
        <translation>Мережа</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="320"/>
        <source>Test Ethernet network connection</source>
        <translation>Тест мережевого з&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="328"/>
        <location filename="../src/siowindow.cpp" line="717"/>
        <source>Ready</source>
        <translation>Готовий</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="409"/>
        <source>WARNING, MAY EXPLODE!</source>
        <translation>ШУХЕР! МОЖЕ БАБАХНУТИ!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="410"/>
        <source>Well, not really... ;)

But using this program might be dangerous for this computer.

It is not recommended to run these tests on a production machine, as they could damage it (if its condition is not good), resulting in data loss.</source>
        <translation>Ну , не зовсім... ;)

Але використання цієї програми може бути небезпечним для цього комп&apos;ютера.

Не рекомендується використовувати ці тести при виробництві, т.я. це може пошкодити обладнання (при поганому стані), що призведе до втрати даних. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="425"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="426"/>
        <source>&amp;Save log to file</source>
        <translation>Зберегти &amp;звіт у файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="438"/>
        <source>&amp;View</source>
        <translation>&amp;Перегляд</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="439"/>
        <source>&amp;Full screen</source>
        <translation>&amp;Повний екран</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="444"/>
        <source>Flat &amp;buttons</source>
        <translation>Плоскі &amp;кнопки </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="452"/>
        <source>&amp;Tests</source>
        <translation>&amp;Тести</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="453"/>
        <source>&amp;Start</source>
        <translation>&amp;Старт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="456"/>
        <source>&amp;Stop</source>
        <translation>&amp;Стоп</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="466"/>
        <source>&amp;Help</source>
        <translation>&amp;Довідка</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="467"/>
        <source>Visit &amp;website</source>
        <translation>Відвідати &amp;веб-сайт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="473"/>
        <source>About &amp;Qt</source>
        <translation>Про &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="474"/>
        <source>Show Qt&apos;s version information</source>
        <translation>Показати інформацію про версію Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="478"/>
        <source>What is Free Software?</source>
        <translation>Що таке вільне ПЗ?</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="482"/>
        <source>&amp;About...</source>
        <translation>Пр&amp;о програму...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="500"/>
        <source>Wrong test duration</source>
        <translation>Хибна тривалість теста</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="501"/>
        <source>0 time!!</source>
        <translation>Час 0!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="508"/>
        <source>CPU load</source>
        <translation>Навантаження процесора</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="537"/>
        <source>Disks</source>
        <translation>Диски</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="562"/>
        <source>Network ports</source>
        <translation>Мережеві порти</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="576"/>
        <source>No tests selected in configuration. Tests not started.</source>
        <translation>Не вибрані тести в налаштуваннях. Тести не запущено.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="583"/>
        <source>Testing</source>
        <translation>Тестування</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="605"/>
        <source>%1 (Running)</source>
        <translation>%1 (Працює)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="606"/>
        <source>Tests started at %1 on %2</source>
        <translation>Тести запущено об %1 %2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="610"/>
        <source>Tests started. Duration: %1h %2m.</source>
        <translation>Тести запущені. Тривалість: %1г %2х.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="622"/>
        <source>Running tests...</source>
        <translation>Тести працюють...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="648"/>
        <source>Tests stopped</source>
        <translation>Тести зупинено</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="712"/>
        <source>Tests stopped after %1h %2m %3s.</source>
        <translation>Тести зупинено через %1г %2х %3с.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="755"/>
        <source>Tests finished</source>
        <translation>Тести завершено</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="756"/>
        <source>Tests ended after %n hour(s)</source>
        <translation>
            <numerusform>Тести завершені через %n годину</numerusform>
            <numerusform>Тести завершені через %n години</numerusform>
            <numerusform>Тести завершені через %n годин</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="758"/>
        <source>and %n minute(s).</source>
        <translation>
            <numerusform>та %n хвилину.</numerusform>
            <numerusform>та %n хвилини.</numerusform>
            <numerusform>та %n хвилин.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="784"/>
        <source>Save log to a file</source>
        <translation>Зберегти звіт у файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="786"/>
        <source>Log files (*.log);; All files (*)</source>
        <translation>Файли звітів (*.log);; Усі файли (*)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="796"/>
        <source>Error saving log</source>
        <translation>Помилка збереження</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="797"/>
        <source>There was an error saving the log to %1:

%2</source>
        <translation>Виникла помилка збереження звіту в %1:

%2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="853"/>
        <source>Information about the Qt Framework</source>
        <translation>Інформація про фреймворк Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="871"/>
        <source>About StressItOut</source>
        <translation>Про StressItOut </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="876"/>
        <source>StressItOut provides several modules to allow testing and stressing some or all the components of the computer.&lt;br&gt;These tests are intended to be run on newly-built computers or in order to verify stability after some hardware changes, like overclocking.&lt;br&gt;&lt;br&gt;It is NOT recommended to run these tests on a production (regular/serious use) computer, they could damage it, resulting in data loss.&lt;br&gt;&lt;br&gt;Use carefully.</source>
        <translation>StressItOut надає деякі модулі для стрес-тестування деяких чи всіх компонентів комп&apos;ютера.&lt;br&gt;Ці тести призначені для використання на щойно-зібраних комп&apos;ютерах чи для перевірки стабільності післе будь-яких апаратних змін, наприклад, розгону.&lt;br&gt;&lt;br&gt;НЕ РЕКОМЕНДУ&apos;ТMСЯ до застосування в виробництві (регулярному/частому використанні) комп&apos;ютерів, т.я. можливі пошкодження, що призведе до втрати даних.&lt;br&gt;&lt;br&gt;Використовуйте обережно.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="883"/>
        <source>Application icon based on one of Oxygen&apos;s icons: http://www.oxygen-icons.org/ (LGPL license)</source>
        <translation>Піктограми базуються на піктограмах Oxygen: http://www.oxygen-icons.org/ (LGPL license) </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="885"/>
        <source>English translation by JanKusanagi.</source>
        <comment>Translators: change this string with your corresponding language and name</comment>
        <translation>Український переклад - Моцьо Геннадій (drool@altlinux.ru).</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="887"/>
        <source>Visit http://jancoding.wordpress.com/stressitout/ for more information.</source>
        <translation>Відвідайте http://jancoding.wordpress.com/stressitout/ для детальної інформації.</translation>
    </message>
</context>
<context>
    <name>SensorsManager</name>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="25"/>
        <source>You should see sensor values here.</source>
        <translation>Тут Вы можете бачити показання датчиків. </translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="70"/>
        <source>Chip %1</source>
        <translation>Чіп %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="71"/>
        <source>Address: %1</source>
        <translation>Адреса: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="72"/>
        <source>Path: %1</source>
        <translation>Шлях: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="73"/>
        <source>Prefix: %1</source>
        <translation>Префікс: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="77"/>
        <source>Features:</source>
        <translation>Датчики:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="94"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="148"/>
        <source>No sensors detected, or not properly configured.
See lm_sensors documentation for more information.

You&apos;ll probably need to run sensors-detect.</source>
        <translation>Датчики не знайдено, або їх не налаштовано.
Ознайомтесь з документацією lm_sensors для детальної інформації.

Можливо, треба запустити sensors-detect.</translation>
    </message>
</context>
<context>
    <name>Test2d</name>
    <message>
        <location filename="../src/test2d.cpp" line="26"/>
        <source>2D Test</source>
        <translation>Тест 2D </translation>
    </message>
</context>
<context>
    <name>Test3d</name>
    <message>
        <location filename="../src/test3d.cpp" line="26"/>
        <source>3D Test</source>
        <translation>Тест 3D </translation>
    </message>
</context>
<context>
    <name>TestCPU</name>
    <message>
        <location filename="../src/testcpu.cpp" line="26"/>
        <source>CPU load</source>
        <translation>Тест процесора</translation>
    </message>
    <message>
        <location filename="../src/testcpu.cpp" line="92"/>
        <source>Threads: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operations: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Потоків: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Операції: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestDisk</name>
    <message>
        <location filename="../src/testdisk.cpp" line="27"/>
        <source>Disk Test</source>
        <translation>Тест диску</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="52"/>
        <source>No disks detected.</source>
        <translation>Диски не знайдено.</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="99"/>
        <source>Read: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Drive: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Читання: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Пристрій: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestMem</name>
    <message>
        <location filename="../src/testmem.cpp" line="26"/>
        <source>Memory Test</source>
        <translation>Тест пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../src/testmem.cpp" line="85"/>
        <source>Verified: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Region: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Перевірено: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Область: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Помилки: &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestOptical</name>
    <message>
        <location filename="../src/testoptical.cpp" line="26"/>
        <source>CD/DVD Test</source>
        <translation>Тест CD/DVD </translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="55"/>
        <source>No optical drives.</source>
        <translation>Немає оптичних приводів.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="103"/>
        <source>Optical: Reached end of disc. Restarting.</source>
        <translation>Оптичний диск: Досягнуто кінець диска. Перезапуск.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="112"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Read: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Loops: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%4&lt;/b&gt;</source>
        <translation>Привід: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Читання: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Прохід: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Помилок: &lt;b&gt;%4&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="123"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Привід: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Помилка: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestSerial</name>
    <message>
        <location filename="../src/testserial.cpp" line="28"/>
        <source>Serial Test</source>
        <translation>Тест COM-портів</translation>
    </message>
    <message>
        <location filename="../src/testserial.cpp" line="98"/>
        <source>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ports:&lt;b&gt;</source>
        <translation>Байт: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Порти:&lt;b&gt;</translation>
    </message>
</context>
</TS>
