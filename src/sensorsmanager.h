/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef SENSORSMANAGER_H
#define SENSORSMANAGER_H

#include <QObject>
#include <QString>

#include <QDebug>


// libsensors
#include <sensors/sensors.h>


class SensorsManager : public QObject
{
    Q_OBJECT

public:
    explicit SensorsManager(QObject *parent = 0);
    ~SensorsManager();

    void getAllSensorsInfo();
    
signals:
    
public slots:
    QString sensorsFullInfo();


private:
    int sensorsAvailable;
    QString sensorsInfoString;
};

#endif // SENSORSMANAGER_H
