/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MACHINEINFOTABLE_H
#define MACHINEINFOTABLE_H

#include <QDesktopWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QResizeEvent>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QNetworkInterface>

#include <QDebug>

#include "sensorsmanager.h"


class MachineInfoTable : public QTableWidget
{
    Q_OBJECT

public:
    MachineInfoTable();
    ~MachineInfoTable();

    void getCPU();
    void getMemory();
    void getDisks();
    void getOptical();
    void getNetworkInterfaces();
    void getResolution();
    void getKernel();
    void getUptime();

    QString cpu();
    QString memory();
    QStringList disks();
    QStringList disksDev();
    QStringList optical();
    QStringList networkInterfaces();
    QString resolution();
    QString kernel();
    QString uptime();

    QString sensorsInfoText();

protected:
    virtual void resizeEvent(QResizeEvent *event);


private:
    // Store detected stuff here
    QString dCPU;
    QString dTotalMemory;
    QString dFreeMemory;
    QStringList dDisks;
    QStringList dDisksDev;
    QStringList dOptical;
    QStringList dNetworkInterfaces;
    QString dResolution;
    QString dKernel;
    QString dUptime;


    SensorsManager *sensorsManager;
};

#endif // MACHINEINFOTABLE_H
