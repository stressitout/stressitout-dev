# StressItOut
# Copyright 2010-2015 JanKusanagi <jancoding@gmx.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
# -----------------------------
# Project created by QtCreator
# -----------------------------

QT += core gui opengl network
greaterThan(QT_MAJOR_VERSION, 4) QT += widgets


TARGET = stressitout
TEMPLATE = app
SOURCES += src/main.cpp \
    src/siowindow.cpp \
    src/testcpu.cpp \
    src/testmem.cpp \
    src/testserial.cpp \
    src/test2d.cpp \
    src/testdisk.cpp \
    src/machineinfotable.cpp \
    src/testoptical.cpp \
    src/logger.cpp \
    src/test3d.cpp \
    src/sensorsmanager.cpp
HEADERS += src/siowindow.h \
    src/testcpu.h \
    src/testmem.h \
    src/testserial.h \
    src/test2d.h \
    src/testdisk.h \
    src/machineinfotable.h \
    src/testoptical.h \
    src/logger.h \
    src/test3d.h \
    src/sensorsmanager.h
OTHER_FILES += README \
    LICENSE \
    CHANGELOG \
    stressitout.desktop \
    INSTALL
TRANSLATIONS += translations/stressitout_es.ts \
    translations/stressitout_ca.ts \
    translations/stressitout_uk.ts \
    translations/stressitout_ru.ts
RESOURCES += sio.qrc

LIBS += -lsensors -lGLU
