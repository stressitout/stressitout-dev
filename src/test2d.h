/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef TEST2D_H
#define TEST2D_H

#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QTimer>
#include <QString>
#include <QtOpenGL>
#include <QGLWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QPen>

#include <QDebug>


class PaintWidget : public QGLWidget
{
    Q_OBJECT

public:
    PaintWidget(QWidget *parent);
    ~PaintWidget();

public slots:
    void paint();

protected:
    virtual void paintGL();


private:
    int paintW;
    int paintH;

    long operations;


};





class Test2d : public QGroupBox
{
    Q_OBJECT

public:
    Test2d();
    ~Test2d();

public slots:

signals:
    void log(QString logString);


private:
    QLabel *infoLabel;
    QVBoxLayout *layout;
    QTimer *testTimer;

    PaintWidget *paintWidget;

    //long operations;

};


#endif // TEST2D_H
