<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>MachineInfoTable</name>
    <message>
        <location filename="../src/machineinfotable.cpp" line="35"/>
        <source>Machine specifications</source>
        <translation>Спецификация машины</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CPU</source>
        <translation>Процессор</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Memory</source>
        <translation>Память</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Disks</source>
        <translation>Диски</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CD/DVD</source>
        <translation>CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Network</source>
        <translation>Сеть</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="55"/>
        <source>No optical drives found</source>
        <translation>Оптических устройств не найдено</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="310"/>
        <source>%1  (MAC %2)</source>
        <translation>%1  (MAC %2)</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="413"/>
        <source>%1 days, %2 hours, %3 minutes, %4 seconds</source>
        <translation>%1 д, %2 ч, %3 м, %4 с</translation>
    </message>
</context>
<context>
    <name>SIOWindow</name>
    <message>
        <location filename="../src/siowindow.cpp" line="34"/>
        <source>Initializing...</source>
        <translation>Инициализация...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="51"/>
        <source>&amp;Run Tests</source>
        <translation>&amp;Запустить тесты</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="53"/>
        <source>S&amp;ensors</source>
        <translation>С&amp;енсоры</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="55"/>
        <source>&amp;Log</source>
        <translation>&amp;Лог</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="57"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Конфигурация</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="126"/>
        <source>Tests not started</source>
        <translation>Тест не запущен</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="156"/>
        <source>R&amp;un for</source>
        <translation>За&amp;пустить на</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="160"/>
        <source> hours</source>
        <translation> час.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="163"/>
        <source>Duration of the tests, hours</source>
        <translation>Длительность тестов, часов</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="168"/>
        <source> minutes</source>
        <translation> мин.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="171"/>
        <source>Duration of the tests, minutes</source>
        <translation>Длительность тестов, минут</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="180"/>
        <source>&amp;Start tests</source>
        <translation>&amp;Старт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="185"/>
        <source>&amp;Stop tests</source>
        <translation>&amp;Стоп</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="194"/>
        <location filename="../src/siowindow.cpp" line="430"/>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="208"/>
        <source>Preparing sensors information...</source>
        <translation>Подготовка информации с сенсоров...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="225"/>
        <source>%1 version %2 started with PID %3, on %4.</source>
        <translation>%1 версия %2 запущена с PID %3 %4.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="232"/>
        <source>Kernel: </source>
        <translation>Ядро: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="235"/>
        <source>System uptime: </source>
        <translation>Система работает: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="239"/>
        <source>CPU:</source>
        <translation>Процессор:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="240"/>
        <source>Memory:</source>
        <translation>Память:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="241"/>
        <source>Hard disks:
</source>
        <translation>Жесткие диски:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="242"/>
        <source>Optical drives: %1</source>
        <translation>Оптические приводы: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="244"/>
        <source>No optical drives found</source>
        <translation>Оптических устройств не найдено</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="245"/>
        <source>Network interfaces:</source>
        <translation>Сетевые интерфейсы:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="249"/>
        <source>Screen resolution: </source>
        <translation>Разрешение экрана: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="250"/>
        <source>Ideal CPU thread count: %1</source>
        <translation>Кол-во идеальных потоков процессора: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="254"/>
        <source>libsensors version: %1</source>
        <translation>версия libsensors: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="269"/>
        <source>CPU</source>
        <translation>Процессор</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="270"/>
        <source>Generate a high processing load to stress the CPU.</source>
        <translation>Создание высокой загрузки для стресса процессора.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="276"/>
        <source> threads</source>
        <translation> потоков</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="280"/>
        <location filename="../src/siowindow.cpp" line="516"/>
        <source>Memory</source>
        <translation>Память</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="281"/>
        <source>Test RAM.</source>
        <translation>Тест памяти.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="285"/>
        <location filename="../src/siowindow.cpp" line="523"/>
        <source>2D graphics</source>
        <translation>Графика 2D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="286"/>
        <source>OpenGL 2D graphics drawing test.</source>
        <translation>Тест графики OpenGL 2D. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="290"/>
        <location filename="../src/siowindow.cpp" line="530"/>
        <source>3D graphics</source>
        <translation>Графика 3D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="291"/>
        <source>OpenGL 3D graphics test.</source>
        <translation>Тест графики OpenGL 3D. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="295"/>
        <source>Hard Disk drives</source>
        <translation>Жесткие диски</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="296"/>
        <source>Test hard disk drives.</source>
        <translation>Тест жестких дисков.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="300"/>
        <location filename="../src/siowindow.cpp" line="544"/>
        <source>Optical drives</source>
        <translation>Оптические приводы</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="301"/>
        <source>Test CD, DVD or Bluray drives.</source>
        <translation>Тест приводов CD, DVD или Bluray. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="305"/>
        <location filename="../src/siowindow.cpp" line="551"/>
        <source>Serial ports</source>
        <translation>Последовательные порты</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="306"/>
        <source>Test transmission on serial (COM) ports. Requires a loopback plug.</source>
        <translation>Тест передачи в COM-порт. Нужна заглушка loopback. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="313"/>
        <location filename="../src/siowindow.cpp" line="558"/>
        <source>Parallel ports</source>
        <translation>Параллельные порты</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="314"/>
        <source>Test transmission on parallel (LPT) ports. Requires a loopback plug.</source>
        <translation>Тест передачи в LPT-порт. Нужна заглушка loopback.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="319"/>
        <source>Network</source>
        <translation>Сеть</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="320"/>
        <source>Test Ethernet network connection</source>
        <translation>Тест сетевого соединения</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="328"/>
        <location filename="../src/siowindow.cpp" line="717"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="409"/>
        <source>WARNING, MAY EXPLODE!</source>
        <translation>АТАС! МОЖЕТ РВАНУТЬ!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="410"/>
        <source>Well, not really... ;)

But using this program might be dangerous for this computer.

It is not recommended to run these tests on a production machine, as they could damage it (if its condition is not good), resulting in data loss.</source>
        <translation>Ну , не совсем... ;)

Но использование этой программы может быть опасным для этого компьютера.

Не рекомендуется использовать эти тесты при производстве, т.к. это может повредить оборудование (при плохом состоянии), что повлечет потерю данных. </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="425"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="426"/>
        <source>&amp;Save log to file</source>
        <translation>Сохранить &amp;лог в файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="438"/>
        <source>&amp;View</source>
        <translation>&amp;Просмотр</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="439"/>
        <source>&amp;Full screen</source>
        <translation>&amp;Полный экран</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="444"/>
        <source>Flat &amp;buttons</source>
        <translation>Плоские &amp;кнопки </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="452"/>
        <source>&amp;Tests</source>
        <translation>&amp;Тесты</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="453"/>
        <source>&amp;Start</source>
        <translation>&amp;Старт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="456"/>
        <source>&amp;Stop</source>
        <translation>&amp;Стоп</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="466"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="467"/>
        <source>Visit &amp;website</source>
        <translation>Посетить &amp;вэб-сайт</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="473"/>
        <source>About &amp;Qt</source>
        <translation>О &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="474"/>
        <source>Show Qt&apos;s version information</source>
        <translation>Показать информацию о версии Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="478"/>
        <source>What is Free Software?</source>
        <translation>Что такое свободное ПО?</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="500"/>
        <source>Wrong test duration</source>
        <translation>Неверная длительность теста</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="501"/>
        <source>0 time!!</source>
        <translation>Время 0!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="508"/>
        <source>CPU load</source>
        <translation>Загрузка процессора</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="537"/>
        <source>Disks</source>
        <translation>Диски</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="562"/>
        <source>Network ports</source>
        <translation>Сетевые порты</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="576"/>
        <source>No tests selected in configuration. Tests not started.</source>
        <translation>Не выбраны тесты в настройках. Тесты не запущены.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="583"/>
        <source>Testing</source>
        <translation>Тестирование</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="605"/>
        <source>%1 (Running)</source>
        <translation>%1 (Работает)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="606"/>
        <source>Tests started at %1 on %2</source>
        <translation>Тесты запущены в %1 %2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="610"/>
        <source>Tests started. Duration: %1h %2m.</source>
        <translation>Тесты запущены. Длительность: %1ч %2м.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="622"/>
        <source>Running tests...</source>
        <translation>Тесты работают...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="648"/>
        <source>Tests stopped</source>
        <translation>Тесты остановлены</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="712"/>
        <source>Tests stopped after %1h %2m %3s.</source>
        <translation>Тесты остановлены через %1ч %2м %3с.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="755"/>
        <source>Tests finished</source>
        <translation>Тесты завершены</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="756"/>
        <source>Tests ended after %n hour(s)</source>
        <translation>
            <numerusform>Тесты завершены через %n час</numerusform>
            <numerusform>Тесты завершены через %n часа</numerusform>
            <numerusform>Тесты завершены через %n часов</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="758"/>
        <source>and %n minute(s).</source>
        <translation>
            <numerusform>и %n минуту.</numerusform>
            <numerusform>и %n минуты.</numerusform>
            <numerusform>и %n минут.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="784"/>
        <source>Save log to a file</source>
        <translation>Сохранить лог в файл</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="786"/>
        <source>Log files (*.log);; All files (*)</source>
        <translation>Файлы логов (*.log);; Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="796"/>
        <source>Error saving log</source>
        <translation>Ошибка сохранения</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="797"/>
        <source>There was an error saving the log to %1:

%2</source>
        <translation>Возникла ошибка сохранения лога в %1:

%2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="853"/>
        <source>Information about the Qt Framework</source>
        <translation>Информация о фреймворке Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="871"/>
        <source>About StressItOut</source>
        <translation>О StressItOut </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="876"/>
        <source>StressItOut provides several modules to allow testing and stressing some or all the components of the computer.&lt;br&gt;These tests are intended to be run on newly-built computers or in order to verify stability after some hardware changes, like overclocking.&lt;br&gt;&lt;br&gt;It is NOT recommended to run these tests on a production (regular/serious use) computer, they could damage it, resulting in data loss.&lt;br&gt;&lt;br&gt;Use carefully.</source>
        <translation>StressItOut предоставляет некоторые модули для стресс-тестирования некоторых или всех компонентов компьютера.&lt;br&gt;Эти тесты предназначены для использования на только-что собранных компьютерах либо для проверки стабильности после каких-либо аппаратных изменений, например, разгона.&lt;br&gt;&lt;br&gt;НЕ РЕКОМЕНДУЕТСЯ к использованию в производстве (регулярному/частому использованию) компьютеров, т.к. возможны повреждения, что повлечет потерю данных.&lt;br&gt;&lt;br&gt;Используйте осторожно.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="883"/>
        <source>Application icon based on one of Oxygen&apos;s icons: http://www.oxygen-icons.org/ (LGPL license)</source>
        <translation>Иконки приложения базируются на иконках Oxygen: http://www.oxygen-icons.org/ (LGPL license) </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="885"/>
        <source>English translation by JanKusanagi.</source>
        <comment>Translators: change this string with your corresponding language and name</comment>
        <translation>Русский перевод - Моцьо Геннадий (drool@altlinux.ru).</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="887"/>
        <source>Visit http://jancoding.wordpress.com/stressitout/ for more information.</source>
        <translation>Посетите http://jancoding.wordpress.com/stressitout/ для детальной информации.</translation>
    </message>
</context>
<context>
    <name>SensorsManager</name>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="25"/>
        <source>You should see sensor values here.</source>
        <translation>Здесь Вы можете видеть показания датчиков. </translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="70"/>
        <source>Chip %1</source>
        <translation>Чип %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="71"/>
        <source>Address: %1</source>
        <translation>Адрес: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="72"/>
        <source>Path: %1</source>
        <translation>Путь: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="73"/>
        <source>Prefix: %1</source>
        <translation>Префикс: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="77"/>
        <source>Features:</source>
        <translation>Датчики:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="94"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="148"/>
        <source>No sensors detected, or not properly configured.
See lm_sensors documentation for more information.

You&apos;ll probably need to run sensors-detect.</source>
        <translation>Сенсоры не обнаружены, либо неверно настроены.
Ознакомьтесь с документацией lm_sensors для детальной информации.

Возможно, нужно запустить sensors-detect.</translation>
    </message>
</context>
<context>
    <name>Test2d</name>
    <message>
        <location filename="../src/test2d.cpp" line="26"/>
        <source>2D Test</source>
        <translation>Тест 2D </translation>
    </message>
</context>
<context>
    <name>Test3d</name>
    <message>
        <location filename="../src/test3d.cpp" line="26"/>
        <source>3D Test</source>
        <translation>Тест 3D </translation>
    </message>
</context>
<context>
    <name>TestCPU</name>
    <message>
        <location filename="../src/testcpu.cpp" line="26"/>
        <source>CPU load</source>
        <translation>Тест процессора</translation>
    </message>
    <message>
        <location filename="../src/testcpu.cpp" line="92"/>
        <source>Threads: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operations: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Потоков: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Операции: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestDisk</name>
    <message>
        <location filename="../src/testdisk.cpp" line="27"/>
        <source>Disk Test</source>
        <translation>Тест диска</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="52"/>
        <source>No disks detected.</source>
        <translation>Диски не обнаружены.</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="99"/>
        <source>Read: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Drive: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Чтение: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Устройство: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestMem</name>
    <message>
        <location filename="../src/testmem.cpp" line="26"/>
        <source>Memory Test</source>
        <translation>Тест памяти</translation>
    </message>
    <message>
        <location filename="../src/testmem.cpp" line="85"/>
        <source>Verified: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Region: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Проверено: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Область: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Ошибки: &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestOptical</name>
    <message>
        <location filename="../src/testoptical.cpp" line="26"/>
        <source>CD/DVD Test</source>
        <translation>Тест CD/DVD </translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="55"/>
        <source>No optical drives.</source>
        <translation>Нет оптических приводов.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="103"/>
        <source>Optical: Reached end of disc. Restarting.</source>
        <translation>Оптический диск: Достингут конец диска. Перезапуск.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="112"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Read: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Loops: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%4&lt;/b&gt;</source>
        <translation>Привод: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Чтение: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Проход: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Ошибок: &lt;b&gt;%4&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="123"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Привод: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ошибка: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestSerial</name>
    <message>
        <location filename="../src/testserial.cpp" line="28"/>
        <source>Serial Test</source>
        <translation>Тест COM-портов</translation>
    </message>
    <message>
        <location filename="../src/testserial.cpp" line="98"/>
        <source>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ports:&lt;b&gt;</source>
        <translation>Байт: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Порты:&lt;b&gt;</translation>
    </message>
</context>
</TS>
