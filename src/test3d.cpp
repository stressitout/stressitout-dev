/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "test3d.h"


Test3d::Test3d()
{
    this->setTitle(tr("3D Test"));

    paintWidget3d = new PaintWidget3d(this);

    layout = new QVBoxLayout(this);
    layout->addWidget(paintWidget3d);

    this->setLayout(layout);


    testTimer = new QTimer(this);
    testTimer->setInterval(20);  // evey 20 msec, repaint (50 fps)
    connect(testTimer, SIGNAL(timeout()), paintWidget3d, SLOT(paint()));
    testTimer->start();


    qDebug() << "Test3d widget created";
}





Test3d::~Test3d()
{
    testTimer->stop();
    delete paintWidget3d;
    paintWidget3d = NULL;


    qDebug() << "Test3d widget destroyed";
}






/*
 *
 * PaintWidget3d class methods
 *
 */




PaintWidget3d::PaintWidget3d(QWidget *parent) : QGLWidget(parent)
{
    this->setMinimumSize(192, 192);

    this->resize(192,192); // tmp?

    paintW = this->width() - 6;   // because of the 3 pixel border on each side
    paintH = this->height() - 6;

    operations = 0;

    qDebug() << "PaintWidget created" << paintW << paintH;
}

PaintWidget3d::~PaintWidget3d()
{
    qDebug() << "PaintWidget destroyed";
}

void PaintWidget3d::paint()
{

    rotAngle += 1.0; // turn 5 degrees in X axis, each frame

    // move the object back and forth, closer, then further away
    if (moveCloser)
    {
        zPos += 0.01f;
        if (zPos > -2.0f)
        {
            moveCloser = !moveCloser;
        }
    }
    else
    {
        zPos -= 0.01f;
        if (zPos < -6.0f)
        {
            moveCloser = !moveCloser;
        }
    }


    this->updateGL();
}


/*
 * OpenGL initialization
 */
void PaintWidget3d::initializeGL()
{
    qDebug() << "initializing GL";

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


    this->rotAngle = 0.0f;
    this->zPos = -3.0f;
    this->moveCloser = false;
}


void PaintWidget3d::resizeGL(int w, int h)
{
    qDebug() << "resizing GL";
    glViewport(0, 0, (GLint)w, (GLint)h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)w/(GLfloat)h,
                   0.1f, 100.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}



/*
 * Paint some 3D primitives
 * Called when updating from testTimer
 */
void PaintWidget3d::paintGL()
{
    /*
    glEnable(GL_LIGHTING);
    GLfloat light[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light);
    glEnable(GL_LIGHT1);
    */

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glTranslatef(0.0f, 0.0f, zPos);

    glRotatef(rotAngle, 0.0f, 1.0f, 0.0f);


    // front face
    glBegin(GL_TRIANGLES);

     glColor3f(1.0f, 0.1f, 0.1f);
     glVertex3f( 0.0f, 1.0f, 0.0f); // top vertex

     glColor3f(0.1f, 1.0f, 0.1f);
     glVertex3f(-1.0f,-1.0f, 1.0f); // bottom left

     glColor3f(0.1f, 0.1f, 1.0f);
     glVertex3f( 1.0f,-1.0f, 1.0f); // bottom right

    glEnd();


    // right face
    glBegin(GL_TRIANGLES);

     glColor3f(0.5f, 0.4f, 0.4f);
     glVertex3f( 0.0f, 1.0f, 0.0f);

     glColor3f(0.1f, 0.4f, 0.9f);
     glVertex3f( 1.0f,-1.0f, 1.0f);

     glColor3f(0.3f, 0.2f, 0.6f);
     glVertex3f( 1.0f,-1.0f, -1.0f);

    glEnd();

    // back face
    glBegin(GL_TRIANGLES);

     glColor3f(0.0f, 0.8f, 0.2f);
     glVertex3f( 0.0f, 1.0f, 0.0f);

     glColor3f(0.2f, 0.7f, 0.4f);
     glVertex3f( 1.0f,-1.0f, -1.0f);

     glColor3f(0.3f, 0.6f, 0.5f);
     glVertex3f( -1.0f,-1.0f, -1.0f);

    glEnd();

    // left face
    glBegin(GL_TRIANGLES);

     glColor3f(0.1f, 0.6f, 0.8f);
     glVertex3f( 0.0f, 1.0f, 0.0f);

     glColor3f(0.0f, 0.1f, 0.6f);
     glVertex3f( -1.0f,-1.0f, -1.0f);

     glColor3f(0.1f, 0.8f, 0.6f);
     glVertex3f( -1.0f,-1.0f, 1.0f);

    glEnd();

    // FIXME: draw bottom square?
}
