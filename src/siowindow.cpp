/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "siowindow.h"


// Main window, constructor: Initial setup of the main window
SIOWindow::SIOWindow()
{
    QSettings settings;
    firstRun = settings.value("MainWindow/firstRun", true).toBool();

    // set window title, icon and size
    this->setWindowIcon(QIcon(":/icon/64x64/stressitout.png"));
    this->setMinimumSize(680, 600);
    this->resize(settings.value("MainWindow/size", QSize(900,540)).toSize());
    this->statusBar()->showMessage(tr("Initializing..."));
    testsRunning = false;


    // Set up the main menu and its actions
    createMenus();


    // Set up central widget for the main window: a QTabWidget with 4 tabs
    this->centralWidget = new QTabWidget();

    this->runTabWidget = new QWidget();
    this->logTabWidget = new Logger();
    this->sensorsTabWidget = new QTextEdit();
    this->confTabWidget = new QWidget();

    centralWidget->addTab(runTabWidget,  QIcon::fromTheme("system-run"),
                          tr("&Run Tests"));
    centralWidget->addTab(sensorsTabWidget, QIcon::fromTheme("utilities-system-monitor"),
                          tr("S&ensors"));
    centralWidget->addTab(logTabWidget,  QIcon::fromTheme("text-x-log"),
                          tr("&Log"));
    centralWidget->addTab(confTabWidget, QIcon::fromTheme("configure"),
                          tr("&Configuration"));

    //centralWidget->setMovable(true);  // movable tabs
    //centralWidget->setTabPosition(QTabWidget::West);  // position of the tabs
    //centralWidget->setDocumentMode(true);  // removes borders
    this->setCentralWidget(centralWidget);



    /*
     *
     * Set up the "Run tests" tab
     *
     */
    this->topLayout = new QHBoxLayout();
    topLayout->setAlignment(Qt::AlignTop);

    this->topRightLayout = new QVBoxLayout();
    topRightLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);

    this->middleLayout = new QHBoxLayout();
    middleLayout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);


    this->bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignBottom);
    bottomLayout->setSpacing(8);

    this->bottomLeftLayout = new QHBoxLayout();
    this->bottomLeftLayout->setAlignment(Qt::AlignLeft);


    runTabTopWidget = new QWidget();
    runTabTopWidget->setLayout(topLayout);
    runTabMiddleWidget = new QWidget();
    runTabMiddleWidget->setLayout(middleLayout);


    centralSplitter = new QSplitter(Qt::Vertical, this);
    centralSplitter->setChildrenCollapsible(false);
    centralSplitter->addWidget(runTabTopWidget);
    centralSplitter->addWidget(runTabMiddleWidget);


    // Add all sub-layouts to the central (main) one
    this->centralLayout = new QVBoxLayout();
    centralLayout->addWidget(centralSplitter, 7); // stretch 7/8
    centralLayout->addLayout(bottomLayout,    1); // stretch 1/8


    this->runTabWidget->setLayout(centralLayout);

    // end of layouts



    // Machine information table
    qDebug() << "creating MachineInfoTable (OS/2 will have problems here)";
    machineInfoTable = new MachineInfoTable();



    // Elapsed time label, initial text: 00:00:00 with spaces surrounding to make the label bigger
    elapsedTimeLabel = new QLabel("<b><font size=\"+4\">&nbsp;&nbsp;&nbsp;&nbsp;00:00:00&nbsp;&nbsp;&nbsp;&nbsp;</font></b>");
    elapsedTimeLabel->setAlignment(Qt::AlignCenter);
    elapsedTimeLabel->setFrameStyle(QFrame::Raised | QFrame::StyledPanel);
    this->topRightLayout->addWidget(elapsedTimeLabel);


    startingTimeLabel = new QLabel(tr("Tests not started"));
    startingTimeLabel->setAlignment(Qt::AlignTop);
    this->topRightLayout->addWidget(startingTimeLabel);

    elapsedTimeTimer = new QTimer();
    elapsedTimeTimer->setInterval(1000);
    connect(elapsedTimeTimer, SIGNAL(timeout()), this, SLOT(updateElapsedTime()));


    this->tmpLabel = new QLabel("");
    tmpLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    //tmpLabel->setWordWrap(true);  // the wordwrap makes the label vertically huge!
    tmpLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::MinimumExpanding);
    this->topRightLayout->addWidget(tmpLabel);


    this->topLayout->addWidget(machineInfoTable);
    this->topLayout->addLayout(topRightLayout);

    testPlaceholderLabel = new QLabel("*");
    testPlaceholderLabel->setAlignment(Qt::AlignCenter);
    testPlaceholderLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                        QSizePolicy::MinimumExpanding);
    this->middleLayout->addWidget(testPlaceholderLabel);


    // now the stuff at the bottom of the window

    this->bottomLayout->addLayout(bottomLeftLayout);

    testDurationLabel = new QLabel(tr("R&un for"), this);
    this->bottomLeftLayout->addWidget(testDurationLabel);

    testDurationHoursBox = new QSpinBox();
    testDurationHoursBox->setSuffix(tr(" hours"));
    testDurationHoursBox->setRange(0, 999);  // max 999h
    testDurationHoursBox->setValue(settings.value("TestsConfig/durationHours", 0).toInt());
    testDurationHoursBox->setToolTip(tr("Duration of the tests, hours"));
    this->bottomLeftLayout->addWidget(testDurationHoursBox);
    this->testDurationLabel->setBuddy(testDurationHoursBox);

    testDurationMinBox = new QSpinBox();
    testDurationMinBox->setSuffix(tr(" minutes"));
    testDurationMinBox->setRange(0, 59);
    testDurationMinBox->setValue(settings.value("TestsConfig/durationMin", 15).toInt());
    testDurationMinBox->setToolTip(tr("Duration of the tests, minutes"));
    this->bottomLeftLayout->addWidget(testDurationMinBox);




    // Little space before the Start and Stop buttons
    this->bottomLayout->addSpacing(24);

    startTestButton = new QPushButton(tr("&Start tests"));
    startTestButton->setIcon(QIcon::fromTheme("media-playback-start"));
    connect(startTestButton, SIGNAL(clicked()), startAction, SLOT(trigger()));
    this->bottomLayout->addWidget(startTestButton);

    stopTestButton = new QPushButton(tr("&Stop tests"));
    stopTestButton->setIcon(QIcon::fromTheme("media-playback-stop"));
    stopTestButton->setDisabled(true); // Disabled at startup
    connect(stopTestButton, SIGNAL(clicked()), stopAction, SLOT(trigger()));
    this->bottomLayout->addWidget(stopTestButton);

    // Little space before the Quit button
    this->bottomLayout->addSpacing(24);

    quitButton = new QPushButton(tr("&Quit"));
    quitButton->setIcon(QIcon::fromTheme("application-exit"));

    connect(quitButton, SIGNAL(clicked()), quitAction, SLOT(trigger()));
    this->bottomLayout->addWidget(quitButton);



    /*
     *
     * Set up the "sensors" tab
     *
     */
    this->sensorsTabWidget->setReadOnly(true);
    sensorsTabWidget->setText(tr("Preparing sensors information..."));
    //sensorsTabWidget->setFontPointSize(sensorsTabWidget->fontPointSize()+1); // TMP

    sensorsInfoTimer = new QTimer();
    sensorsInfoTimer->setInterval(2500); // refresh every 2.5 seconds
    connect(sensorsInfoTimer, SIGNAL(timeout()),
            this, SLOT(updateSensorsInfo()));
    sensorsInfoTimer->start();




    /*
     *
     * Set up the "log" tab
     *
     */
    logTabWidget->log(tr("%1 version %2 started with PID %3, on %4.")
                      .arg(qApp->applicationName())
                      .arg(qApp->applicationVersion())
                      .arg(qApp->applicationPid())
                      .arg(QDate::currentDate().toString("dd-MMM-yy")));

    machineInfoTable->getKernel();
    logTabWidget->log(tr("Kernel: ") + machineInfoTable->kernel());

    machineInfoTable->getUptime();
    logTabWidget->log(tr("System uptime: ") + machineInfoTable->uptime());


    /* disable these logging instructions below, for OS/2 until bug is fixed TMPFIX */
    logTabWidget->log(tr("CPU:") + " " + machineInfoTable->cpu());
    logTabWidget->log(tr("Memory:") + " " + machineInfoTable->memory());
    logTabWidget->log(tr("Hard disks:\n") + machineInfoTable->disks().join("\n"));
    logTabWidget->log(tr("Optical drives: %1").arg(machineInfoTable->optical().length() > 0
                                             ? machineInfoTable->optical().join(", ")
                                             : tr("No optical drives found")));
    logTabWidget->log(tr("Network interfaces:")  + "\n" + machineInfoTable->networkInterfaces().join("\n"));


    machineInfoTable->getResolution();
    logTabWidget->log(tr("Screen resolution: ") + machineInfoTable->resolution());
    logTabWidget->log(tr("Ideal CPU thread count: %1").arg(QThread::idealThreadCount()));
    /* disable these logging instructions above, for OS/2 until bug is fixed TMPFIX */


    logTabWidget->log(tr("libsensors version: %1").arg(libsensors_version));




    /*
     *
     * Set up the "configuration" tab
     *
     */
    confTabLayout = new QGridLayout();
    this->confTabWidget->setLayout(confTabLayout);

    settings.beginGroup("TestsConfig");

    cpuCheckBox = new QCheckBox(tr("CPU"));
    cpuCheckBox->setToolTip(tr("Generate a high processing load to stress the CPU."));
    cpuCheckBox->setChecked(settings.value("cpu", true).toBool());
    confTabLayout->addWidget(cpuCheckBox, 0, 0);
    cpuThreadsConfig = new QSpinBox(this);
    cpuThreadsConfig->setRange(1, 256);  // min: 1 thread, max 256 threads
    cpuThreadsConfig->setValue(settings.value("cpuThreads", 16).toInt());
    cpuThreadsConfig->setSuffix(tr(" threads"));
    confTabLayout->addWidget(cpuThreadsConfig, 0, 1);


    memCheckBox = new QCheckBox(tr("Memory"));
    memCheckBox->setToolTip(tr("Test RAM."));
    memCheckBox->setChecked(settings.value("mem", true).toBool());
    confTabLayout->addWidget(memCheckBox, 1, 0);

    g2dCheckBox = new QCheckBox(tr("2D graphics"));
    g2dCheckBox->setToolTip(tr("OpenGL 2D graphics drawing test."));
    g2dCheckBox->setChecked(settings.value("g2d", true).toBool());
    confTabLayout->addWidget(g2dCheckBox, 2, 0);

    g3dCheckBox = new QCheckBox(tr("3D graphics"));
    g3dCheckBox->setToolTip(tr("OpenGL 3D graphics test."));
    g3dCheckBox->setChecked(settings.value("g3d", true).toBool());
    confTabLayout->addWidget(g3dCheckBox, 3, 0);

    diskCheckBox = new QCheckBox(tr("Hard Disk drives"));
    diskCheckBox->setToolTip(tr("Test hard disk drives."));
    diskCheckBox->setChecked(settings.value("disk", false).toBool());
    confTabLayout->addWidget(diskCheckBox, 4, 0);

    opticCheckBox = new QCheckBox(tr("Optical drives"));
    opticCheckBox->setToolTip(tr("Test CD, DVD or Bluray drives."));
    opticCheckBox->setChecked(settings.value("optical", false).toBool());
    confTabLayout->addWidget(opticCheckBox, 5, 0);

    serialCheckBox = new QCheckBox(tr("Serial ports"));
    serialCheckBox->setToolTip(tr("Test transmission on serial (COM) ports. Requires a loopback plug."));
    serialCheckBox->setChecked(settings.value("serial", false).toBool());
    confTabLayout->addWidget(serialCheckBox, 6, 0);
    serialConfig = new QLineEdit(this);
    serialConfig->setText(settings.value("serialPorts", "/dev/ttyS0 /dev/ttyS1").toString());
    confTabLayout->addWidget(serialConfig, 6, 1);

    parallelCheckBox = new QCheckBox(tr("Parallel ports"));
    parallelCheckBox->setToolTip(tr("Test transmission on parallel (LPT) ports. Requires a loopback plug."));
    parallelCheckBox->setChecked(settings.value("parallel", false).toBool());
    parallelCheckBox->setDisabled(true);             // TMPFIX this test does not exist yet
    confTabLayout->addWidget(parallelCheckBox, 7, 0);

    networkCheckBox = new QCheckBox(tr("Network"));
    networkCheckBox->setToolTip(tr("Test Ethernet network connection"));
    networkCheckBox->setChecked(settings.value("network", false).toBool());
    networkCheckBox->setDisabled(true);              // TMPFIX this test does not exist yet
    confTabLayout->addWidget(networkCheckBox, 8, 0);


    settings.endGroup();

    this->statusBar()->showMessage(tr("Ready"));
    this->viewFlatButtonsAction->setChecked(settings.value("MainWindow/flatButtons", false).toBool());

    qDebug() << "SIO window created";





    //tmp (qt4 only; Qt5 uses QStandardPaths
    //qDebug() << "(TMP) system/user paths";
    //qDebug() << QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    //qDebug() << QDesktopServices::storageLocation(QDesktopServices::HomeLocation);
    //qDebug() << QDesktopServices::storageLocation(QDesktopServices::TempLocation);
}




// main window, destructor
SIOWindow::~SIOWindow()
{
    qDebug() << "SIO window destroyed";
}




/*
 * Store application settings upon exit
 *
 */
void SIOWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;                    // Appname, etc are already specified in main()

    settings.beginGroup("MainWindow");
    settings.setValue("firstRun", false);
    settings.setValue("size", this->size());
    settings.setValue("flatButtons", viewFlatButtonsAction->isChecked());
    settings.endGroup();

    settings.beginGroup("TestsConfig");
    settings.setValue("cpu",         cpuCheckBox->isChecked());
    settings.setValue("cpuThreads",  cpuThreadsConfig->value());
    settings.setValue("mem",         memCheckBox->isChecked());
    settings.setValue("g2d",         g2dCheckBox->isChecked());
    settings.setValue("g3d",         g3dCheckBox->isChecked());
    settings.setValue("disk",        diskCheckBox->isChecked());
    settings.setValue("optical",     opticCheckBox->isChecked());
    settings.setValue("serial",      serialCheckBox->isChecked());
    settings.setValue("serialPorts", serialConfig->text());
    settings.setValue("parallel",    parallelCheckBox->isChecked());
    settings.setValue("network",     networkCheckBox->isChecked());

    settings.setValue("durationHours", this->testDurationHoursBox->value());
    settings.setValue("durationMin", this->testDurationMinBox->value());
    settings.endGroup();

    event->accept();
    qDebug() << "closeEvent(): Configuration saved";
}






// Define the basic text for the title bar
void SIOWindow::setBaseTitleBarText(QString base)
{
    this->baseTitleBarText = base;
    this->setWindowTitle(baseTitleBarText);
}


// warning message for first time run
void SIOWindow::showFirstTimeWarning()
{
    if (firstRun)
    {
        QMessageBox::information(this, tr("WARNING, MAY EXPLODE!"),
                                 tr("Well, not really... ;)\n\n"
                                    "But using this program might be dangerous for this computer.\n\n"
                                    "It is not recommended to run these tests on a production machine, "
                                    "as they could damage it (if its condition is not good), resulting in data loss."));
    }
}



/*
 * Create the main menu and its actions: File, Test, Help...
 *
 */
void SIOWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));  //  Save log, Quit (print results?)
    saveLogAction = new QAction(QIcon::fromTheme("document-save-as"), tr("&Save log to file"), this);
    connect(saveLogAction, SIGNAL(triggered()), this, SLOT(saveLog()));
    fileMenu->addAction(saveLogAction);
    fileMenu->addSeparator();
    quitAction = new QAction(QIcon::fromTheme("application-exit"), tr("&Quit"), this);
    quitAction->setShortcut(QKeySequence::Quit);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));
    fileMenu->addAction(quitAction);


    /* View menu */

    viewMenu = menuBar()->addMenu(tr("&View"));  // Fullscreen
    viewFullscreenAction = new QAction(QIcon::fromTheme("view-fullscreen"), tr("&Full screen"), this);
    viewFullscreenAction->setShortcut(Qt::Key_F11);
    viewFullscreenAction->setCheckable(true);
    connect(viewFullscreenAction, SIGNAL(toggled(bool)), this, SLOT(toggleFullscreen(bool)));
    viewMenu->addAction(viewFullscreenAction);
    viewFlatButtonsAction = new QAction(QIcon::fromTheme("insert-button"), tr("Flat &buttons"), this);
    viewFlatButtonsAction->setCheckable(true);
    connect(viewFlatButtonsAction, SIGNAL(toggled(bool)), this, SLOT(toggleFlatButtons(bool)));
    viewMenu->addAction(viewFlatButtonsAction);


    /* Tests menu */

    testsMenu = menuBar()->addMenu(tr("&Tests"));  //  Start, Stop
    startAction = new QAction(QIcon::fromTheme("media-playback-start"), tr("&Start"), this);
    connect(startAction, SIGNAL(triggered()), this, SLOT(startTests()));
    testsMenu->addAction(startAction);
    stopAction = new QAction(QIcon::fromTheme("media-playback-stop"),  tr("&Stop"), this);
    stopAction->setDisabled(true);
    connect(stopAction, SIGNAL(triggered()), this, SLOT(stopTests()));
    testsMenu->addAction(stopAction);


    menuBar()->addSeparator();

    /* Help menu */

    helpMenu = menuBar()->addMenu(tr("&Help"));  // About, About Qt
    visitWebAction = new QAction(QIcon::fromTheme("internet-web-browser"), tr("Visit &website"), this);
    connect(visitWebAction, SIGNAL(triggered()), this, SLOT(openWebsite()));
    helpMenu->addAction(visitWebAction);

    helpMenu->addSeparator();

    aboutQtAction = new QAction(QIcon::fromTheme("system-help"), tr("About &Qt"), this);
    aboutQtAction->setStatusTip(tr("Show Qt's version information"));
    connect(aboutQtAction, SIGNAL(triggered()), this, SLOT(aboutQt()));
    helpMenu->addAction(aboutQtAction);

    aboutFreeSoftwareAction = new QAction(QIcon::fromTheme("package-x-generic"), tr("What is Free Software?"), this);
    connect(aboutFreeSoftwareAction, SIGNAL(triggered()), this, SLOT(aboutFS()));
    helpMenu->addAction(aboutFreeSoftwareAction);

    aboutAction = new QAction(QIcon::fromTheme("computer"), tr("&About..."), this);
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(aboutSIO()));
    helpMenu->addAction(aboutAction);
}






/*
 * Triggered from the "start tests" button or "test > start" menu
 *
 */
void SIOWindow::startTests()
{
    if (testDurationHoursBox->value() == 0 && testDurationMinBox->value() == 0)
    {
        QMessageBox::warning(this, tr("Wrong test duration"),
                                   tr("0 time!!"));
        return;
    }

    QString testsString("");
    if (this->cpuCheckBox->isChecked())
    {
        testsString += tr("CPU load") + ", ";
        cpuWidget = new TestCPU(cpuThreadsConfig->value());
        connect(cpuWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(cpuWidget);

    }
    if (this->memCheckBox->isChecked())
    {
        testsString += tr("Memory") + ", ";
        memWidget = new TestMem();
        connect(memWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(memWidget);
    }
    if (this->g2dCheckBox->isChecked())
    {
        testsString += tr("2D graphics") + ", ";
        g2dWidget = new Test2d();
        connect(g2dWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(g2dWidget);
    }
    if (this->g3dCheckBox->isChecked())
    {
        testsString += tr("3D graphics") + ", ";
        g3dWidget = new Test3d();
        connect(g3dWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(g3dWidget);
    }
    if (this->diskCheckBox->isChecked())
    {
        testsString += tr("Disks") + ", ";
        diskWidget = new TestDisk(machineInfoTable->disksDev());
        connect(diskWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(diskWidget);
    }
    if (this->opticCheckBox->isChecked())
    {
        testsString += tr("Optical drives") + ", ";
        opticalWidget = new TestOptical(machineInfoTable->optical());
        connect(opticalWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(opticalWidget);
    }
    if (this->serialCheckBox->isChecked())
    {
        testsString += tr("Serial ports") + ", ";
        serialWidget = new TestSerial(serialConfig->text());
        connect(serialWidget, SIGNAL(log(QString)), logTabWidget, SLOT(log(QString)));
        this->middleLayout->addWidget(serialWidget);
    }
    if (this->parallelCheckBox->isChecked())
    {
        testsString += tr("Parallel ports") + ", ";
    }
    if (this->networkCheckBox->isChecked())
    {
        testsString += tr("Network ports") + ", ";
    }




    /*
     * List of checks needed has been verified.
     * Now, if some were selected, they will be started
     *
    */
    if (testsString.isEmpty())  // if no test were selected in configuration
    {
        this->tmpLabel->setText("No tests selected!!");
        logTabWidget->log(tr("No tests selected in configuration. Tests not started."));
    }
    else                        // else, if some tests were selected, start them
    {
        testsString.append("@");    // add the @ so that removing the end is easier
        testsString.remove(", @");  // (ugly hack)

        testsString = tr("Testing") + " " + testsString;
        this->tmpLabel->setText(testsString);

        // Hide the placeholder
        this->testPlaceholderLabel->hide();

        // Ensure MachineInfoTable gets scrollbar if needed, forcing a resizeEvent()
        this->machineInfoTable->setColumnWidth(0, 999);


        testDurationHoursBox->setDisabled(true);  // disable some controls
        testDurationMinBox->setDisabled(true);
        startTestButton->setDisabled(true);
        stopTestButton->setEnabled(true);    // and enable others
        quitButton->setDisabled(true);

        startAction->setDisabled(true);      // in the menu too
        stopAction->setEnabled(true);


        confTabWidget->setDisabled(true);

        this->setWindowTitle(QString(tr("%1 (Running)")).arg(baseTitleBarText));
        startingTimeLabel->setText(QString(tr("Tests started at %1 on %2"))
                                   .arg(QTime::currentTime().toString("hh:mm:ss"))
                                   .arg(QDate::currentDate().toString("dd/MM/yy")));
        // Add starting time info to the log too
        logTabWidget->log(tr("Tests started. Duration: %1h %2m.")
                          .arg(testDurationHoursBox->value())
                          .arg(testDurationMinBox->value()));
        logTabWidget->log(testsString);

        elapsedSec  = 0;
        elapsedMin  = 0;
        elapsedHour = 0;
        updateElapsedTime();
        this->elapsedTimeTimer->start();


        this->statusBar()->showMessage(tr("Running tests..."));
        qDebug() << "Starting tests...";
        testsRunning = true;
    }
}



/*
 * triggered from the "stop tests" button or "test > stop" menu
 *
 */
void SIOWindow::stopTests()
{
    testDurationHoursBox->setEnabled(true);
    testDurationMinBox->setEnabled(true);
    startTestButton->setEnabled(true);
    stopTestButton->setDisabled(true);
    quitButton->setEnabled(true);

    startAction->setEnabled(true);
    stopAction->setDisabled(true);

    confTabWidget->setEnabled(true);        // re-enable configuration tab
    this->setWindowTitle(baseTitleBarText);

    this->tmpLabel->setText("-- " + tr("Tests stopped") + " --");

    // Show the placeholder again
    this->testPlaceholderLabel->show();

    // Ensure MachineInfoTable repaints itself, forcing a resizeEvent()
    this->machineInfoTable->setColumnWidth(0, 999);


    this->elapsedTimeTimer->stop();


    // Delete widgets
    if (cpuCheckBox->isChecked())
    {
        if (cpuWidget)
        {
            delete cpuWidget;  //TMPFIX
        }
    }
    if (memCheckBox->isChecked())
    {
        if (memWidget)
        {
            delete memWidget;  //TMPFIX
        }
    }
    if (g2dCheckBox->isChecked())
    {
        if (g2dWidget)
        {
            delete g2dWidget;  //TMPFIX
        }
    }
    if (g3dCheckBox->isChecked())
    {
        if (g3dWidget)
        {
            delete g3dWidget;  //TMPFIX
        }
    }
    if (diskCheckBox->isChecked())
    {
        if (diskWidget)
        {
            delete diskWidget;  //TMPFIX
        }
    }
    if (opticCheckBox->isChecked())
    {
        if (opticalWidget)
        {
            delete opticalWidget;  //TMPFIX
        }
    }
    if (serialCheckBox->isChecked())
    {
        if (serialWidget)
        {
            delete serialWidget;  //TMPFIX
        }
    }


    logTabWidget->log(tr("Tests stopped after %1h %2m %3s.")
                      .arg(elapsedHour)
                      .arg(elapsedMin)
                      .arg(elapsedSec));

    this->statusBar()->showMessage(tr("Ready"));
    qDebug() << "Stopping tests...";
    testsRunning = false;
}




/*
 * triggered from elapsedTimeTimer, every second, while tests are running
 *
 */
void SIOWindow::updateElapsedTime()
{
    ++elapsedSec;
    if (elapsedSec == 60)
    {
        elapsedSec = 0;
        ++elapsedMin;
        if (elapsedMin == 60)
        {
            elapsedMin = 0;
            ++elapsedHour;
        }
    }

    elapsedTimeLabel->setText("<b><font size=\"+4\">" +
                              QString("%1:%2:%3")
                              .arg(elapsedHour, 2, 10, QChar('0'))
                              .arg(elapsedMin,  2, 10, QChar('0'))
                              .arg(elapsedSec,  2, 10, QChar('0')) +
                              "</font></b>");

    if (elapsedHour == testDurationHoursBox->value() && elapsedMin == testDurationMinBox->value())
    {
        qDebug() << "Time's up! Test ended!";
        stopTests();
        // FIX: we really need to stop the tests but leave the widgets with the results to be seen
        QMessageBox::information(this, tr("Tests finished"),
                                 tr("Tests ended after %n hour(s)", "", testDurationHoursBox->value())
                                 + " "
                                 + tr("and %n minute(s).", "", testDurationMinBox->value())
                                 );
    }

}



/*
 * Triggered from sensorsInfoTimer, every few seconds
 *
 */
void SIOWindow::updateSensorsInfo()
{
    sensorsTabWidget->setText(machineInfoTable->sensorsInfoText());
}




/*
 * Triggered from the File menu; save log to a file.
 */
void SIOWindow::saveLog()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Save log to a file"),
                                            QDir::homePath() + "/StressItOut-" + QDate::currentDate().toString("yyyyMMdd") + ".log", // ".log" required under GTK filedialog
                                            tr("Log files (*.log);; All files (*)"));
    if (!fileName.isEmpty())
    {
        QFile logFile(fileName);
        logFile.open(QIODevice::WriteOnly);
        QString logContents(this->logTabWidget->toPlainText());

        logContents.append("\n");
        logFile.write(logContents.toLocal8Bit());

        if (logFile.error() != QFile::NoError)
        {
            QMessageBox::warning(this, tr("Error saving log"),
                                 tr("There was an error saving the log to %1:\n\n%2").arg(fileName).arg(logFile.errorString()));
        }
        logFile.close();
    }
}



/*
 * Triggered from the View menu; Toggle fullscreen mode
 */
void SIOWindow::toggleFullscreen(bool state)
{
    if (state)
    {
        this->showFullScreen();     // TMPFIX: should save original size.
    }                               // Max size is saved if quitting while fullscreen is on
    else
    {
        this->showNormal();
    }

}




/*
 * Triggered from the View menu; Toggle flat buttons
 */
void SIOWindow::toggleFlatButtons(bool state)
{
    this->startTestButton->setFlat(state);
    this->stopTestButton->setFlat(state);
    this->quitButton->setFlat(state);
    qDebug() << "toggleFlatButtons()" << state;
}





/*
 * Triggered from the Help menu; Show SIO website on default web browser
 */
void SIOWindow::openWebsite()
{
    QDesktopServices::openUrl(QUrl("http://jancoding.wordpress.com/stressitout/"));
}


/*
 * Triggered from the Help menu; Show "About Qt" dialog
 */
void SIOWindow::aboutQt()
{
    QMessageBox::aboutQt(this, tr("Information about the Qt Framework"));
}


/*
 * Triggered from the Help menu; Show FSF's information about Free Software
 */
void SIOWindow::aboutFS()
{
    QDesktopServices::openUrl(QUrl("http://www.gnu.org/philosophy/free-sw.html"));
}


/*
 * Triggered from the Help menu; Show typical "about" dialog with a short description about SIO
 */
void SIOWindow::aboutSIO()
{
    QMessageBox::about(this, tr("About StressItOut"),
                       QString("<strong>%1</strong><br>").arg(baseTitleBarText) +
                       "(C) 2010-2015  JanKusanagi<br><br>"
                       "<a href=\"http://jancoding.wordpress.com/stressitout/\">"
                       "http://jancoding.wordpress.com/stressitout/</a>"
                       "<br /><br />" +
                       tr("StressItOut provides several modules to allow "
                          "testing and stressing some or all the components "
                          "of the computer."
                          "<br>"
                          "These tests are intended to be run on newly-built "
                          "computers or in order to verify stability after "
                          "some hardware changes, like overclocking."
                          "<br><br>"
                          "It is NOT recommended to run these tests on a "
                          "production (regular/serious use) computer, as they "
                          "could damage it, resulting in data loss."
                          "<br><br>"
                          "Use carefully.")
                       + "<br /><br />"
                       + tr("Application icon based on one of Oxygen's icons: "
                            "http://www.oxygen-icons.org/ (LGPL license)")
                       + "<br /><br />"
                       + tr("English translation by JanKusanagi.",
                            "Translators: change this string with your "
                            "corresponding language and name")
                       + "<br /><br />"
                       + tr("Visit http://jancoding.wordpress.com/stressitout/ "
                            "for more information."));
}
