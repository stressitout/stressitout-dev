/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <QApplication>
#include <QtGui>
#include <QString>
#include <iostream>

#include "siowindow.h"



#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
void customMessageHandlerQt4(QtMsgType type, const char *msg)
{
    Q_UNUSED(type)
    Q_UNUSED(msg)

    // Do nothing

    return;
}
#else
void customMessageHandlerQt5(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type)
    Q_UNUSED(context)
    Q_UNUSED(msg)

    // Do nothing

    return;
}
#endif


int main(int argc, char *argv[])
{
    QApplication SIOapp(argc, argv);

    SIOapp.setApplicationName("StressItOut");
    SIOapp.setApplicationVersion("0.2-dev");
    SIOapp.setOrganizationName("JanCoding"); // will be used as folder name under ~/.config
    SIOapp.setOrganizationDomain("jancoding.wordpress.com");

    std::cout << QString("%1 v%2 - JanKusanagi 2010-2015\n")
                 .arg(SIOapp.applicationName())
                 .arg(SIOapp.applicationVersion()).toStdString();
    std::cout.flush();

    /*
     * Register custom message handler, to hide debug messages unless specified
     *
     */
     if (qApp->arguments().contains("--debug", Qt::CaseInsensitive))
     {
         qDebug() << "Debug messages enabled";
     }
     else
     {
         qDebug() << "To see debug messages while running, use --debug";

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
         qInstallMsgHandler(customMessageHandlerQt4);
#else
         qInstallMessageHandler(customMessageHandlerQt5);
#endif

     }


    /*
     * Load language files if available, based on system locale
     *
     * Run with different languages:
     *       $ LANG=$YOUR_LANGUAGE ./stressItOut
     */
    QTranslator translator;
    QString languageFile;
    // get LANG environment variable
    languageFile = QString(":/translations/stressitout_%1").arg(qgetenv("LANG").constData());
    qDebug() << "Language file:" << languageFile;

    translator.load(languageFile);
    SIOapp.installTranslator(&translator);


    SIOWindow sioWindow;
    sioWindow.setBaseTitleBarText(QString("%1 - v%2")
                                  .arg(SIOapp.applicationName())
                                  .arg(SIOapp.applicationVersion()));
    sioWindow.show();

    sioWindow.showFirstTimeWarning();

    return SIOapp.exec();
}
