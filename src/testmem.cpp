/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "testmem.h"


TestMem::TestMem()
{
    this->setTitle(tr("Memory Test"));

    infoLabel = new QLabel("MEM");
    infoLabel->setAlignment(Qt::AlignLeft);
    infoLabel->setMinimumSize(192, 192);
    layout = new QVBoxLayout(this);
    layout->addWidget(infoLabel);

    this->setLayout(layout);

    operations = 0;

    numBuffer = 0;
    oldBuffer = 1;

    allocSize = 1024*1024*8;   // allocate 8 MiB
    errorCount = 0;

    buffer[numBuffer] = (unsigned char *)malloc(allocSize);  // TMPFIX: add error control!
    if (buffer[numBuffer] == NULL)
    {
        qDebug() << "Error allocating memory for memtest!";
    }
    bufferPosition = buffer[numBuffer];


    testTimer = new QTimer(this);
    testTimer->setInterval(20);  // evey 20 msec
    connect(testTimer, SIGNAL(timeout()), this, SLOT(memCheck()));
    testTimer->start();

    qDebug() << "TestMem widget created";
}




TestMem::~TestMem()
{
    testTimer->stop();
    delete testTimer;
    testTimer = NULL;

    free(buffer[numBuffer]); // buffer[oldBuffer] is always freed inside TestMem::memCheck()

    qDebug() << "TestMem widget destroyed";
}



/*
 * Data copying in memory to check integrity
 * Called repeatedly from testTimer signal
 */
void TestMem::memCheck()
{
    char memAddr[20];

    sprintf(memAddr, "%p", bufferPosition);
    infoLabel->setText(QString(tr("Verified: <b>%1 KiB</b><br>"
                                  "Region: <b>%2</b><br>"
                                  "Errors: <b>%3</b>"))
                      .arg(operations/1024)
                      .arg(memAddr)
                      .arg(errorCount));

    for (byteCounter = 0; byteCounter != 8192; ++byteCounter) // 8 KiB
    {
        if (operations & 1)
        {
            *bufferPosition = /*'a';*/170; // if odd, 10101010
        }
        else
        {
            *bufferPosition = /*'b';*/85;  // if even, 01010101
        }

        ++operations;
        ++bufferPosition;
        if (bufferPosition == buffer[numBuffer] + allocSize)
        {
            break;  // TMPFIX, ugly
        }
    }

    bufferPosition -= 8192; // go back the last 8 KiB to check them
    operations -= 8192;

    // Verify
    for (byteCounter = 0; byteCounter != 8192; ++byteCounter) // 8 KiB
    {
        if (operations & 1)
        {
            if (*bufferPosition != 170/*'a'*/) // if odd, 10101010
            {
                ++errorCount;
                qDebug() << *bufferPosition << QChar(170);
            }
        }
        else
        {
            if (*bufferPosition != 85/*'b'*/)  // if even, 01010101
            {
                ++errorCount;
                qDebug() << *bufferPosition << QChar(85);
            }
        }

        ++operations;
        ++bufferPosition;
        if (bufferPosition == buffer[numBuffer] + allocSize)
        {
            break;  // TMPFIX, ugly
        }
    }


    if (bufferPosition == buffer[numBuffer] + allocSize)
    {
        char buffInfoTMP[100];
        sprintf(buffInfoTMP, "buffer %d: %p  ->  bufferPosition %p, check ended",
                numBuffer, buffer[numBuffer], bufferPosition);
        qDebug() << buffInfoTMP;

        // swap buffers
        qSwap(numBuffer, oldBuffer);

        buffer[numBuffer] = (unsigned char *)malloc(allocSize);
        if (buffer[numBuffer] == NULL)
        {
            qDebug() << "memCheck() Error while allocating more memory!";
        }

        bufferPosition = buffer[numBuffer];

        free(buffer[oldBuffer]);
        qDebug() << "reallocated TestMem::buffer" << numBuffer << oldBuffer;
    }
}
