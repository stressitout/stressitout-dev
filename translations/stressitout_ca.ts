<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ca_ES">
<context>
    <name>MachineInfoTable</name>
    <message>
        <location filename="../src/machineinfotable.cpp" line="35"/>
        <source>Machine specifications</source>
        <translation>Característiques de l&apos;equip</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Disks</source>
        <translation>Discs</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>CD/DVD</source>
        <translation>CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="36"/>
        <source>Network</source>
        <translation>Xarxa</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="55"/>
        <source>No optical drives found</source>
        <translation>No s&apos;han trobat unitats òptiques</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="310"/>
        <source>%1  (MAC %2)</source>
        <translation>%1  (MAC %2)</translation>
    </message>
    <message>
        <location filename="../src/machineinfotable.cpp" line="413"/>
        <source>%1 days, %2 hours, %3 minutes, %4 seconds</source>
        <oldsource>%1 days %2 hours, %3 minutes, %4 seconds</oldsource>
        <translation>%1 díes, %2 hores, %3 minuts, %4 segons</translation>
    </message>
</context>
<context>
    <name>SIOWindow</name>
    <message>
        <location filename="../src/siowindow.cpp" line="34"/>
        <source>Initializing...</source>
        <translation>Inicialitzant...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="51"/>
        <source>&amp;Run Tests</source>
        <translation>&amp;Executar tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="53"/>
        <source>S&amp;ensors</source>
        <translation>Se&amp;nsors</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="55"/>
        <source>&amp;Log</source>
        <translation>&amp;Registre</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="57"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuració</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="126"/>
        <source>Tests not started</source>
        <translation>Tests no iniciats</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="168"/>
        <source> minutes</source>
        <translation> minuts</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="180"/>
        <source>&amp;Start tests</source>
        <translation>&amp;Iniciar tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="185"/>
        <source>&amp;Stop tests</source>
        <translation>At&amp;urar tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="194"/>
        <location filename="../src/siowindow.cpp" line="430"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="156"/>
        <source>R&amp;un for</source>
        <translation>E&amp;xecutar durant</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="160"/>
        <source> hours</source>
        <translation> hores</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="163"/>
        <source>Duration of the tests, hours</source>
        <translation>Duració dels tests, hores</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="171"/>
        <source>Duration of the tests, minutes</source>
        <translation>Duració dels tests, minuts</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="208"/>
        <source>Preparing sensors information...</source>
        <translation>Preparant informació de sensors...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="232"/>
        <source>Kernel: </source>
        <translation>Kernel: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="235"/>
        <source>System uptime: </source>
        <translation>Uptime del sistema: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="249"/>
        <source>Screen resolution: </source>
        <translation>Resolució de pantalla: </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="250"/>
        <source>Ideal CPU thread count: %1</source>
        <translation>Nombre ideal de fils de CPU: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="254"/>
        <source>libsensors version: %1</source>
        <translation>libsensors versió: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="269"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="270"/>
        <source>Generate a high processing load to stress the CPU.</source>
        <translation>Generar una gran càrrega de processament per estressar la CPU.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="276"/>
        <source> threads</source>
        <translation> fils</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="280"/>
        <location filename="../src/siowindow.cpp" line="516"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="281"/>
        <source>Test RAM.</source>
        <translation>Comprovar la RAM.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="285"/>
        <location filename="../src/siowindow.cpp" line="523"/>
        <source>2D graphics</source>
        <translation>Gràfics 2D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="286"/>
        <source>OpenGL 2D graphics drawing test.</source>
        <translation>Test de dibuixat de gràfics 2D OpenGL.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="290"/>
        <location filename="../src/siowindow.cpp" line="530"/>
        <source>3D graphics</source>
        <translation>Gràfics 3D</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="291"/>
        <source>OpenGL 3D graphics test.</source>
        <translation>Test de gràfics 3D OpenGL.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="295"/>
        <source>Hard Disk drives</source>
        <translation>Unitats de disc dur</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="296"/>
        <source>Test hard disk drives.</source>
        <translation>Comprovar unitats de disc dur.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="300"/>
        <location filename="../src/siowindow.cpp" line="544"/>
        <source>Optical drives</source>
        <translation>Unitats óptiques</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="301"/>
        <source>Test CD, DVD or Bluray drives.</source>
        <translation>Comprovar unitats de CD, DVD o Bluray.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="305"/>
        <location filename="../src/siowindow.cpp" line="551"/>
        <source>Serial ports</source>
        <translation>Ports serie</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="306"/>
        <source>Test transmission on serial (COM) ports. Requires a loopback plug.</source>
        <translation>Comprovar la transmisió en ports série (COM). És necessari un connector loopback.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="313"/>
        <location filename="../src/siowindow.cpp" line="558"/>
        <source>Parallel ports</source>
        <translation>Ports paral·lels</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="314"/>
        <source>Test transmission on parallel (LPT) ports. Requires a loopback plug.</source>
        <translation>Comprovar la transmisió en ports paral·lels (LTP). És necessari un connector loopback.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="319"/>
        <source>Network</source>
        <translation>Xarxa</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="320"/>
        <source>Test Ethernet network connection</source>
        <translation>Comprovar la conexió de xarxa Ethernet</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="328"/>
        <location filename="../src/siowindow.cpp" line="717"/>
        <source>Ready</source>
        <translation>Preparat</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="508"/>
        <source>CPU load</source>
        <translation>Càrrega de CPU</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="537"/>
        <source>Disks</source>
        <translation>Discs</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="562"/>
        <source>Network ports</source>
        <translation>Ports de xarxa</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="853"/>
        <source>Information about the Qt Framework</source>
        <translation>Veure informació de la versió de Qt Framework</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="876"/>
        <source>StressItOut provides several modules to allow testing and stressing some or all the components of the computer.&lt;br&gt;These tests are intended to be run on newly-built computers or in order to verify stability after some hardware changes, like overclocking.&lt;br&gt;&lt;br&gt;It is NOT recommended to run these tests on a production (regular/serious use) computer, they could damage it, resulting in data loss.&lt;br&gt;&lt;br&gt;Use carefully.</source>
        <translation>StressItOut proporciona diversos mòduls que permeten provar i estressar alguns o tots els components de l&apos;ordinador.&lt;br&gt;Aquests tests estan pensats per ser executats en ordinadors acabats de muntar, o per verificar l&apos;estabilitat després d&apos;alguns canvis al hardware, com &apos;overclocking&apos;.&lt;br&gt;NO es recomana executar aquests tests en un ordinador de producció (ús regular/seriós), podrien danyar-lo, resultant en pèrdua de dades.&lt;br&gt;Utilitza-ho amb compte.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="883"/>
        <source>Application icon based on one of Oxygen&apos;s icons: http://www.oxygen-icons.org/ (LGPL license)</source>
        <translation>Icona de l&apos;aplicació basada en una de les icones Oxygen: http://www.oxygen-icons.org/ (llicencia LGPL)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="885"/>
        <source>English translation by JanKusanagi.</source>
        <comment>Translators: change this string with your corresponding language and name</comment>
        <translation>Traducció al català per JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="887"/>
        <source>Visit http://jancoding.wordpress.com/stressitout/ for more information.</source>
        <translation>Visita http://jancoding.wordpress.com/stressitout/ per més informació.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="425"/>
        <source>&amp;File</source>
        <translation>&amp;Arxiu</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="426"/>
        <source>&amp;Save log to file</source>
        <translation>&amp;Guardar el registre a un arxiu</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="438"/>
        <source>&amp;View</source>
        <translation>&amp;Veure</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="439"/>
        <source>&amp;Full screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="452"/>
        <source>&amp;Tests</source>
        <translation>&amp;Tests</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="453"/>
        <source>&amp;Start</source>
        <translation>&amp;Iniciar</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="456"/>
        <source>&amp;Stop</source>
        <translation>&amp;Aturar</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="466"/>
        <source>&amp;Help</source>
        <translation>A&amp;juda</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="467"/>
        <source>Visit &amp;website</source>
        <translation>Visitar lloc &amp;web</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="478"/>
        <source>What is Free Software?</source>
        <translation>Què és el sofware lliure?</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;Sobre...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="610"/>
        <source>Tests started. Duration: %1h %2m.</source>
        <translation>Tests iniciats. Duració: %1h %2m.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="648"/>
        <source>Tests stopped</source>
        <translation>Tests aturats</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="712"/>
        <source>Tests stopped after %1h %2m %3s.</source>
        <translation>Els tests s&apos;han aturat després de %1h %2m %3s.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="756"/>
        <source>Tests ended after %n hour(s)</source>
        <translation>
            <numerusform>Els tests han acabat després de %n hora</numerusform>
            <numerusform>Els tests han acabat després de %n hora</numerusform>
            <numerusform>Els tests han acabat després de %n hores</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/siowindow.cpp" line="758"/>
        <source>and %n minute(s).</source>
        <translation>
            <numerusform>i %n minut.</numerusform>
            <numerusform>i %n minut.</numerusform>
            <numerusform>i %n minuts.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="473"/>
        <source>About &amp;Qt</source>
        <translation>Sobre &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="225"/>
        <source>%1 version %2 started with PID %3, on %4.</source>
        <translation>%1 versió %2 iniciat amb PID %3, el %4.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="239"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="240"/>
        <source>Memory:</source>
        <translation>Memoria:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="241"/>
        <source>Hard disks:
</source>
        <translation>Discs durs:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="242"/>
        <source>Optical drives: %1</source>
        <translation>Unitats óptiques: %1</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="244"/>
        <source>No optical drives found</source>
        <translation>No s&apos;han trobat unitats òptiques</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="245"/>
        <source>Network interfaces:</source>
        <translation>Interfícies de xarxa:</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="409"/>
        <source>WARNING, MAY EXPLODE!</source>
        <translation>COMPTE, POT EXPLOTAR!!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="410"/>
        <source>Well, not really... ;)

But using this program might be dangerous for this computer.

It is not recommended to run these tests on a production machine, as they could damage it (if its condition is not good), resulting in data loss.</source>
        <translation>Bé, en realitat no... ;)

Però utilitzar aquest programa pot ser perillós per aquest ordinador.

No es recomana executar aquests tests en una màquina de producció, ja que podrien fer-la malbè (si no està en bones condicions), resultant en una pèrdua de dades.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="444"/>
        <source>Flat &amp;buttons</source>
        <translation>&amp;Botons plans</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="474"/>
        <source>Show Qt&apos;s version information</source>
        <translation>Veure informació de la versió de Qt</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="500"/>
        <source>Wrong test duration</source>
        <translation>Duració de test no vàlida</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="501"/>
        <source>0 time!!</source>
        <translation>Temps 0!!</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="576"/>
        <source>No tests selected in configuration. Tests not started.</source>
        <translation>No hi ha cap test sel·leccionat a la configuració. Tests no iniciats.</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="583"/>
        <source>Testing</source>
        <translation>Provant</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="605"/>
        <source>%1 (Running)</source>
        <translation>%1 (Executant)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="606"/>
        <source>Tests started at %1 on %2</source>
        <translation>Tests iniciats a les %1 el %2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="622"/>
        <source>Running tests...</source>
        <translation>Executant tests...</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="755"/>
        <source>Tests finished</source>
        <translation>Tests finalitzats</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="784"/>
        <source>Save log to a file</source>
        <translation>Guardar registre a un arxiu</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="786"/>
        <source>Log files (*.log);; All files (*)</source>
        <translation>Arxius de registre (*.log);; Tots els arxius (*)</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="796"/>
        <source>Error saving log</source>
        <translation>Error guardant el registre</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="797"/>
        <source>There was an error saving the log to %1:

%2</source>
        <translation>Hi ha hagut un error al guardar el registre a %1:

%2</translation>
    </message>
    <message>
        <location filename="../src/siowindow.cpp" line="871"/>
        <source>About StressItOut</source>
        <translation>Sobre StressItOut</translation>
    </message>
</context>
<context>
    <name>SensorsManager</name>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="25"/>
        <source>You should see sensor values here.</source>
        <translation>Aquí s&apos;hauríen de veure valors dels sensors.</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="70"/>
        <source>Chip %1</source>
        <translation>Xip %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="71"/>
        <source>Address: %1</source>
        <translation>Adreça: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="72"/>
        <source>Path: %1</source>
        <translation>Camí: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="73"/>
        <source>Prefix: %1</source>
        <translation>Prefixe: %1</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="77"/>
        <source>Features:</source>
        <translation>Característiques:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="94"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/sensorsmanager.cpp" line="148"/>
        <source>No sensors detected, or not properly configured.
See lm_sensors documentation for more information.

You&apos;ll probably need to run sensors-detect.</source>
        <translation>No s&apos;ha detectat sensors, o no estan ben configurats.
Mira la documentació de lm_sensors per a més informació.

Probablement necessitis executar sensors-detect.</translation>
    </message>
</context>
<context>
    <name>Test2d</name>
    <message>
        <location filename="../src/test2d.cpp" line="26"/>
        <source>2D Test</source>
        <translation>Test 2D</translation>
    </message>
</context>
<context>
    <name>Test3d</name>
    <message>
        <location filename="../src/test3d.cpp" line="26"/>
        <source>3D Test</source>
        <translation>Test 3D</translation>
    </message>
</context>
<context>
    <name>TestCPU</name>
    <message>
        <location filename="../src/testcpu.cpp" line="26"/>
        <source>CPU load</source>
        <translation>Càrrega de CPU</translation>
    </message>
    <message>
        <location filename="../src/testcpu.cpp" line="92"/>
        <source>Threads: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operations: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Fils: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Operacions: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestDisk</name>
    <message>
        <location filename="../src/testdisk.cpp" line="27"/>
        <source>Disk Test</source>
        <translation>Test de disc</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="52"/>
        <source>No disks detected.</source>
        <translation>No s&apos;han detectat discs.</translation>
    </message>
    <message>
        <location filename="../src/testdisk.cpp" line="99"/>
        <source>Read: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Drive: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Llegit: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Unitat: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestMem</name>
    <message>
        <location filename="../src/testmem.cpp" line="26"/>
        <source>Memory Test</source>
        <translation>Test de memoria</translation>
    </message>
    <message>
        <location filename="../src/testmem.cpp" line="85"/>
        <source>Verified: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Region: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Verificat: &lt;b&gt;%1 KiB&lt;/b&gt;&lt;br&gt;Regió: &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestOptical</name>
    <message>
        <location filename="../src/testoptical.cpp" line="26"/>
        <source>CD/DVD Test</source>
        <translation>Test de CD/DVD</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="55"/>
        <source>No optical drives.</source>
        <translation>No hi ha unitats óptiques.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="103"/>
        <source>Optical: Reached end of disc. Restarting.</source>
        <translation>Óptic: S&apos;ha arribat al final del disc. Reiniciant.</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="112"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Read: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Loops: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%4&lt;/b&gt;</source>
        <translation>Unitat: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Llegit: &lt;b&gt;%2 KiB&lt;/b&gt;&lt;br&gt;Voltes: &lt;b&gt;%3&lt;/b&gt;&lt;br&gt;Errors: &lt;b&gt;%4&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/testoptical.cpp" line="123"/>
        <source>Drive: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Unitat: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Error: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TestSerial</name>
    <message>
        <location filename="../src/testserial.cpp" line="28"/>
        <source>Serial Test</source>
        <translation>Test de port serie</translation>
    </message>
    <message>
        <location filename="../src/testserial.cpp" line="98"/>
        <source>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ports:&lt;b&gt;</source>
        <translation>Bytes: &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;Ports:&lt;b&gt;</translation>
    </message>
</context>
</TS>
