/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "testdisk.h"



TestDisk::TestDisk(QStringList deviceList)
{
    this->setTitle(tr("Disk Test"));

    infoLabel = new QLabel("DISK");
    infoLabel->setAlignment(Qt::AlignLeft);
    infoLabel->setMinimumSize(192, 192);
    layout = new QVBoxLayout(this);
    layout->addWidget(infoLabel);

    this->setLayout(layout);


    operations = 0;
    testTimer = new QTimer(this);


    this->diskList = deviceList;

    if (diskList.size() > 0)
    {
        testTimer->setInterval(1000);  // evey 1000 msec
        connect(testTimer, SIGNAL(timeout()), this, SLOT(diskCheck()));
        testTimer->start();
    }
    else
    {
        infoLabel->setText(tr("No disks detected."));
    }

    qDebug() << "TestDisk widget created for" << deviceList;
}





TestDisk::~TestDisk()
{
    testTimer->stop();
    delete testTimer;
    testTimer = NULL;
    qDebug() << "TestDisk widget destroyed";
}


/*
 * Disk reading and writing
 * User MIGHT need to be on "disk" group in order to run this test,
 * or run as root (!)
 *
 * Called repeatedly from testTimer signal
 */
void TestDisk::diskCheck()
{
    /* TMPFIX:
     *          some tests
     *
     */

    QFile hdd("/dev/" + diskList.at(0)); // FIXME: use all disks or allow selection
    hdd.open(QIODevice::ReadOnly);
    if (hdd.isOpen())
    {
        qDebug() << hdd.fileName() << hdd.read(1024) << hdd.error() << hdd.errorString();
        ++operations;
        hdd.close();
    }
    else
    {
        emit log("Disk: Could not open HDD");
        qDebug() << "could not open" << hdd.fileName() << hdd.errorString();
    }

    infoLabel->setText(QString(tr("Read: <b>%1 KiB</b><br>Drive: <b>%2</b>"))
                      .arg(operations)
                      .arg(hdd.fileName()));

}
