/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "testoptical.h"


TestOptical::TestOptical(QStringList deviceList)
{
    this->setTitle(tr("CD/DVD Test"));

    infoLabel = new QLabel("OPTICAL");
    infoLabel->setAlignment(Qt::AlignLeft);
    infoLabel->setMinimumSize(192, 192);
    layout = new QVBoxLayout(this);
    layout->addWidget(infoLabel);

    this->setLayout(layout);


    operations = 0;
    loops = 0;
    errors = 0;

    testTimer = new QTimer(this);

    this->driveList = deviceList;
    if (driveList.size() > 0)
    {
        drive.setFileName(driveList.at(0)); // FIXME: use all or allow selecting
        drive.open(QIODevice::ReadOnly);

        testTimer->setInterval(10);  // evey 10 msec
        connect(testTimer, SIGNAL(timeout()), this, SLOT(opticalCheck()));
        testTimer->start();
    }
    else
    {
        this->infoLabel->setText(tr("No optical drives."));
    }


    qDebug() << "TestOptical widget created for" << deviceList;
}





TestOptical::~TestOptical()
{
    testTimer->stop();
    delete testTimer;
    testTimer = NULL;

    drive.close();

    qDebug() << "TestOptical widget destroyed";
}


/*
 * CD/DVD reading
 * User MIGHT need to be on "cdrom" group in order to run this test,
 * or run as root (!)
 *
 * Called repeatedly from testTimer signal
 */
void TestOptical::opticalCheck()
{
    int kibToRead = 256;
    QByteArray readStatus;

    if (drive.isOpen())
    {
        readStatus = drive.read(1024 * kibToRead);
        operations += kibToRead;

        //qDebug() << readStatus.length() << drive.pos();

        if (drive.error() == QFile::NoError)
        {
            if (readStatus.length() == 0)  // No error + zero length = end of cd/dvd, so restart
            {
                ++loops;
                drive.seek(0);
                emit log(tr("Optical: Reached end of disc. Restarting."));
            }
        }
        else   // if there are errors with the drive
        {
            qDebug() << "error reading? " << drive.error() << drive.errorString();
            ++errors;
        }

        infoLabel->setText(tr("Drive: <b>%1</b><br>Read: <b>%2 KiB</b><br>Loops: <b>%3</b><br>Errors: <b>%4</b>")
                          .arg(drive.fileName())
                          .arg(operations)
                          .arg(loops)
                          .arg(errors));
    }
    else
    {
        testTimer->stop();
        emit log("Optical: could not open device " + drive.fileName() + " -> " + drive.errorString());

        infoLabel->setText(tr("Drive: <b>%1</b><br>Error: <b>%2</b>")
                          .arg(drive.fileName())
                          .arg(drive.errorString()));
    }

}
