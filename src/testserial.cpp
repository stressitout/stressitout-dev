/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "testserial.h"

/*
 * Main constructor; open ports RW
 */
TestSerial::TestSerial(QString ports)
{
    this->setTitle(tr("Serial Test"));

    infoLabel = new QLabel("SERIAL");
    infoLabel->setAlignment(Qt::AlignLeft);
    infoLabel->setMinimumSize(192, 192);
    layout = new QVBoxLayout(this);
    layout->addWidget(infoLabel);

    this->setLayout(layout);

    this->portList = ports.split(" ");


    port.clear();
    for (int counter=0; counter != this->portList.size(); ++counter)
    {
        port.append(new QFile(portList.at(counter)));
        if (port.at(counter)->open(QIODevice::ReadWrite))
        {
           qDebug() << "Port " << counter << port.at(counter)->fileName() << "open RW";
        }
        else
        {
            qDebug() << "Port " << counter << port.at(counter)->fileName() << "could NOT be opened RW!";
        }
    }


    operations = 0;

    testTimer = new QTimer(this);
    testTimer->setInterval(20);  // evey 20 msec
    connect(testTimer, SIGNAL(timeout()), this, SLOT(serialCheck()));
    testTimer->start();

    qDebug() << "TestSerial widget created";
}





TestSerial::~TestSerial()
{
    testTimer->stop();
    delete testTimer;
    testTimer = NULL;

    for (int counter=0; counter != this->portList.size(); ++counter) // close all used ports
    {
        if (port.at(counter)->isOpen())
        {
            emit log("Closing port");
            qDebug() << "Closing port" << port.at(counter)->fileName();
            port.at(counter)->close();
        }
    }

    qDebug() << "TestSerial widget destroyed";
}


/*
 * Serial transfer check. Loopback needed!
 * Called repeatedly from testTimer signal
 *
 */
void TestSerial::serialCheck()
{
    QByteArray dataPacket("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    QString portsInfo(tr("Bytes: <b>%1</b><br>Ports:<b>"));

    for (int counter=0; counter != this->portList.size(); ++counter)
    {
        if (port.at(counter)->isOpen())
        {
            // WRITE to the port
            if (port.at(counter)->isWritable())
            {
                port.at(counter)->write(dataPacket);  // send data through the port
                port.at(counter)->flush();
                operations += dataPacket.length();
            }

            // READ from the port
            if (port.at(counter)->waitForReadyRead(10)) // HW loopback needed to verify if this works
            {
                qDebug() << "Port" <<  port.at(counter)->fileName() << "is readable and has data";
                if (port.at(counter)->read(dataPacket.length()) == dataPacket)
                {
                    qDebug() << "Port" << port.at(counter)->fileName() << "read successful";
                    operations += dataPacket.length();
                }
                else
                {
                    qDebug() << "Port" << port.at(counter)->fileName() << "read FAILED";
                }
            }
            else
            {
                qDebug() << "Cannot read data from port " << port.at(counter)->fileName();
            }

            portsInfo.append("<br>");
            portsInfo.append(portList.at(counter));
        }
    }
    portsInfo.append("</b>");

    infoLabel->setText(QString(portsInfo).arg(operations));
}
