/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "testcpu.h"


TestCPU::TestCPU(int configuredThreads)
{
    this->setTitle(tr("CPU load"));

    infoLabel = new QLabel("CPU");
    infoLabel->setAlignment(Qt::AlignLeft);
    infoLabel->setMinimumSize(192, 192);
    layout = new QVBoxLayout(this);
    layout->addWidget(infoLabel);

    this->setLayout(layout);

    operations = 0;
    numThreads = configuredThreads;

    for (int threadCount=0; threadCount != numThreads; ++threadCount)
    {
        cpuThread[threadCount] = new CPUThread(threadCount);
        cpuThread[threadCount]->setRunning(true);
        cpuThread[threadCount]->start(QThread::HighPriority);
        qDebug() << "starting thread " << threadCount;
    }


    testTimer = new QTimer(this);
    testTimer->setInterval(500);  // refresh labels evey 500 msec
    connect(testTimer, SIGNAL(timeout()), this, SLOT(cpuBurn()));
    testTimer->start();


    qDebug() << "TestCPU widget created with " << numThreads << " threads";
}





TestCPU::~TestCPU()
{
    for (int threadCount=0; threadCount != numThreads; ++threadCount)
    {
        cpuThread[threadCount]->setRunning(false);
        cpuThread[threadCount]->wait();
        qDebug() << "stopping thread" << threadCount;
    }

    qDebug() << "Stopping cpuTimer...";
    testTimer->stop();
    delete testTimer;
    testTimer = NULL;

    qDebug() << "TestCPU widget destroyed";
}



/*
 * Calculations tu generate high load on the CPU.
 * Called repeatedly from cpuTimer signal
 */
void TestCPU::cpuBurn()
{
    operations = 0;
    for (int threadCount=0; threadCount != numThreads; ++threadCount)
    {
        operations += cpuThread[threadCount]->operationCount();
    }

    infoLabel->setText(QString(tr("Threads: <b>%1</b><br>Operations: <b>%2</b>"))
                       .arg(numThreads).arg(operations));
}




/*
 *
 *  CPUThread class methods
 *
 */
CPUThread::CPUThread(int threadNum)
{
    this->threadNumber = threadNum;
    qDebug() << "CPUThread created" << this->threadNumber;
}

CPUThread::~CPUThread()
{
    qDebug() << "CPUThread destroyed" << this->threadNumber;
}


void CPUThread::run()
{
    this->operations = 0;
    float num;
    float num2;

    num = this->threadNumber * 2.222;


    while (running)
    {
        qsrand(QTime::currentTime().msec());

        num2 = 1.01010101;

        for (int counter = 0; counter != 15000; ++counter)
        {
            num =  qrand() / qrand();
            num += qrand() / (qrand() + num2);
            num -= qrand() * (qrand() + (num2 * 1.020202));
            num += qrand() / (qrand() + (num2 * 2.121212));
            num -= qrand() * (qrand() + (num2 * 1.777777));
            num += qrand() / (qrand() + (num2 * 2.5050505));
            num -= qrand() * (qrand() + num2);
            num += qrand() / (qrand() + num2);
            num += qrand() * num2 * (num != 0 ?  num : 5.050505);
            num += qrand() / num2;
            num += qrand() % 256;
        }

        ++operations;
    }
    qDebug() << "CPUthread ended" << this->threadNumber;
    this->deleteLater();
}


void CPUThread::setRunning(bool state)
{
    this->running = state;
}


qint64 CPUThread::operationCount()
{
    return this->operations;
}
