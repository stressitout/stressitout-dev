/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "test2d.h"


Test2d::Test2d()
{
    this->setTitle(tr("2D Test"));

    paintWidget = new PaintWidget(this);

    layout = new QVBoxLayout(this);
    layout->addWidget(paintWidget);


    this->setLayout(layout);


    testTimer = new QTimer(this);
    testTimer->setInterval(50);  // evey 50 msec, repaint!
    connect(testTimer, SIGNAL(timeout()), paintWidget, SLOT(paint()));
    testTimer->start();


    qDebug() << "Test2d widget created";
}





Test2d::~Test2d()
{
    testTimer->stop();
    delete paintWidget;
    paintWidget = NULL;


    qDebug() << "Test2d widget destroyed";
}






/*
 *
 * PaintWidget class methods
 *
 */




PaintWidget::PaintWidget(QWidget *parent) : QGLWidget(parent)
{
    this->setMinimumSize(192, 192);

    this->resize(192,192); // tmp?

    paintW = this->width() - 6;   // because of the 3 pixel border on each side
    paintH = this->height() - 6;


    operations = 3;  // start from 3 px

    qDebug() << "PaintWidget created" << paintW << paintH;
}

PaintWidget::~PaintWidget()
{
    qDebug() << "PaintWidget destroyed";
}

void PaintWidget::paint()
{
    operations += 2;

    if (operations >= paintH)
    {
        operations = 3;

        //TMP REMOVED  emit log(tr("2D loop restarting"));
        qDebug() << "2D loop restart";
    }

    this->updateGL();
    //qDebug() << "paintwidget::paint";
}


/*
 * Paint some 2D primitives
 * Called when updating from testTimer
 */
void PaintWidget::paintGL()
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::black, 3));

    QRadialGradient radialGrad(100, 100, 100);
    radialGrad.setColorAt(0, Qt::black);
    radialGrad.setColorAt(1, Qt::gray);
    painter.setBrush(radialGrad);

    // paint the background of the 2Dtest widget with a radial black-to-gray gradient
    painter.drawRect(3, 3,
                     paintW, paintH);


    /*
     * Very ugly code, but for now, it's a decent
     * 2D painting test.
     */

    // first square
    QRadialGradient radialGrad2(92, 92, 100);
    radialGrad2.setColorAt(0, Qt::blue);
    radialGrad2.setColorAt(1, Qt::darkBlue);
    painter.setBrush(radialGrad2);
    painter.setPen(QPen(Qt::darkGreen, 2));
    painter.drawRect(operations-8, operations-8, paintW-(operations*2), paintH-(operations*2));

    // second square
    QRadialGradient radialGrad3(108, 108, 100);
    radialGrad3.setColorAt(0, Qt::darkCyan);
    radialGrad3.setColorAt(1, Qt::cyan);
    painter.setBrush(radialGrad3);
    painter.setPen(QPen(Qt::green, 2));
    painter.drawRect(operations+8, operations+8, paintW-(operations*2), paintH-(operations*2));


    //painter.drawPixmap(0,0, 192, 192, QPixmap(":/images/stressitout.png"));


    // upper side X lines
    painter.setPen(QPen(Qt::white, 2));
    painter.drawLine(3, 3, paintW, operations);
    painter.drawLine(3, operations, paintW, 3);

    // lower side X lines
    painter.setPen(QPen(Qt::yellow, 2));
    painter.drawLine(3, paintH, paintW, paintH-operations);
    painter.drawLine(3, paintH-operations, paintW, paintH);


    painter.end();


    //qDebug() << "paintGL" << operations;

}
