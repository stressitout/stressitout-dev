/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

//#include <syscall.h>  // kernel
#include "machineinfotable.h"


/*
 * Create a QTableWidget with information about the computer:
 * CPU type, Memory size, Hard drives, Optical drives...
 *
 */
MachineInfoTable::MachineInfoTable()
{
    this->setRowCount(5);
    this->setColumnCount(1);

    this->setHorizontalHeaderLabels(QStringList(tr("Machine specifications")));
    this->setVerticalHeaderLabels(QStringList(tr("CPU")) << tr("Memory") << tr("Disks") << tr("CD/DVD") << tr("Network"));
    this->setEditTriggers(QTableWidget::NoEditTriggers);

    this->setSizePolicy(QSizePolicy::Minimum,
                        QSizePolicy::MinimumExpanding);

    this->getCPU();
    setItem(0,0, new QTableWidgetItem(QIcon::fromTheme("cpu"),
                                      this->cpu(), 0));
    this->getMemory();
    setItem(0,1, new QTableWidgetItem(QIcon::fromTheme("media-flash"),
                                      this->memory(), 0));
    this->getDisks();
    setItem(0,2, new QTableWidgetItem(QIcon::fromTheme("drive-harddisk"),
                                      this->disks().join("\n"), 0));
    this->getOptical();
    setItem(0,3, new QTableWidgetItem(QIcon::fromTheme("drive-optical"),
                                      this->optical().length() > 0
                                      ? this->optical().join("\n")
                                      : tr("No optical drives found"), 0));
    this->getNetworkInterfaces();
    setItem(0,4, new QTableWidgetItem(QIcon::fromTheme("network-wired"),
                                      this->networkInterfaces().join("\n"), 0));



    // useful?   this->setContentsMargins(0,0,0,0);
    this->resizeColumnsToContents(); // Column width will be resized later as needed,
    this->resizeRowsToContents();    // but this way, row height is correct

    this->setTabKeyNavigation(false); // disable Tab, so the user can navigate the rest of the UI
    this->setSelectionMode(QAbstractItemView::NoSelection); // Don't allow selecting rows

    // lm_sensors stuff (libsensors)    
    sensorsManager = new SensorsManager(this);


    qDebug() << "MachineInfoTable created";
}



MachineInfoTable::~MachineInfoTable()
{
    qDebug() << "MachineInfoTable destroyed";
}



/*
 * Upon resizing of the window, adjust columns to maximum width available
 */
void MachineInfoTable::resizeEvent(QResizeEvent *event)
{
    // Resize the only column (0) to the width of the widget
    // minus the width of the header (CPU, Memory, etc), so it fits nicely
    this->setColumnWidth(0,  this->width() - this->verticalHeader()->width());


    event->accept();
}




/*
 * Get CPU Name from /proc/cpuinfo
 *
 */
void MachineInfoTable::getCPU()
{
    // quite TMPFIX - get processor type
    qDebug() << "getting CPU info";
    QFile cpuInfo("/proc/cpuinfo");
    cpuInfo.open(QIODevice::ReadOnly);
    QString cpuName;
    while (cpuInfo.isOpen())
    {
        qDebug() << "cpuInfo.isOpen = true";
        cpuName = cpuInfo.readLine(128);
        if (cpuName.startsWith("model name", Qt::CaseInsensitive))
        {
            cpuName.remove(QRegExp("model name.*:"));
            cpuName.replace(QRegExp("\\s\\s+"), " ");
            qDebug() << "model name line found and replaced, closing file";
            cpuInfo.close();
        }
    }
    cpuInfo.close();
    qDebug() << cpuName.trimmed();

    this->dCPU = cpuName.trimmed();
}


/*
 * Get total memory from /proc/meminfo
 *
 */
void MachineInfoTable::getMemory()
{
    qDebug() << "getting memory info";
    QFile memInfo("/proc/meminfo");
    memInfo.open(QIODevice::ReadOnly);

    QString totalMemSize;
    QString freeMemSize;

    QString memoryString;

    while (memInfo.isOpen())
    {
        qDebug() << "memInfo.isOpen = true";
        memoryString = memInfo.readLine(100).toLower();

        // Total memory amount
        if (memoryString.startsWith("memtotal:"))
        {
            QRegExp memoryRE("memtotal:\\s*(\\d+)\\s*(.b)"); // MemTotal: (number) (unit: kb, mb...)
            memoryRE.indexIn(memoryString);

            totalMemSize = QString("%1 MiB").arg(memoryRE.cap(1).toInt() / 1024);  // TMPFIX
            qDebug() << "MemTotal line found";
        }

        // Free memory amount
        if (memoryString.startsWith("memfree:"))
        {
            QRegExp memoryRE("memfree:\\s*(\\d+)\\s*(.b)"); // Memfree: (number) (unit: kb, mb...)
            memoryRE.indexIn(memoryString);

            freeMemSize = QString("%1 MiB").arg(memoryRE.cap(1).toInt() / 1024);  // TMPFIX
            qDebug() << "MemFree line found";
        }

        // Both sizes read, closing file
        if (!totalMemSize.isEmpty() && !freeMemSize.isEmpty())
        {
            memInfo.close();
        }

    }

    qDebug() << totalMemSize << " total //" << freeMemSize << "free";

    this->dTotalMemory = totalMemSize;
    this->dFreeMemory  = freeMemSize;
}



/*
 * Get hard disks and partitions from /proc/partitions
 *
 */
void MachineInfoTable::getDisks()
{
    qDebug() << "getting Disks info";
    QFile diskInfo("/proc/partitions");  // See also /proc/scsi/scsi, etc.
    diskInfo.open(QIODevice::ReadOnly);


    //
    // VERY VERY TMPFIX
    //
    QString diskDrives;

    diskDrives = diskInfo.readLine(128);  // Skip the "major minor #blocks name" line
    diskDrives = diskInfo.readLine(128);  // Skip blank line


    this->dDisks.clear();
    this->dDisksDev.clear();
    while (!diskInfo.atEnd())
    {
        diskDrives = diskInfo.readLine(128).trimmed();  // first disk
        qDebug() << diskDrives;

        if (diskDrives.contains(QRegExp(".*\\d$"))) // something, then number, then \n
        {
            qDebug() << "Ends with number, it's a partition, ignoring...";
        }
        else
        {
            QStringList diskDetails = diskDrives.split(QRegExp("\\s+"));

            qint64 capacityLong = diskDetails.at(2).toLongLong() * 1024;


            diskDrives = QString("/dev/%1: %2 GB (%3 GiB)")
                         .arg(diskDetails.at(3))
                         .arg((qreal)capacityLong / (1000 * 1000 * 1000), 0, 'f', 1)  // GB
                         .arg((qreal)capacityLong / (1024 * 1024 * 1024), 0, 'f', 2); // GiB

            this->dDisks.append(diskDrives);
            this->dDisksDev.append(diskDetails.at(3));
        }
    }

    qDebug() << "dDisks:" << this->dDisks << this->dDisksDev;

    diskInfo.close();
}


/*
 * Get optical drives from /proc/sys/dev/cdrom/info
 *
 */
void MachineInfoTable::getOptical()
{
    qDebug() << "getting Optical drives info";
    QFile opticalInfo("/proc/sys/dev/cdrom/info");
    opticalInfo.open(QIODevice::ReadOnly);
    QString opticalDrives;

    bool found = false;

    while (opticalInfo.isOpen())
    {
        qDebug() << "opticalInfo.isOpen = true";
        opticalDrives = opticalInfo.readLine(128);
        if (opticalDrives.startsWith("drive name:", Qt::CaseInsensitive))
        {
            opticalDrives.remove(QRegExp("drive name:"));
            qDebug() << "drive name line found and replaced, closing file";
            found = true;
            opticalInfo.close();
        }
    }
    opticalInfo.close();

    if (found)
    {
        qDebug() << "Some optical drives found; listing...";
        opticalDrives = opticalDrives.trimmed();

        QStringList opticalDrivesNames;
        opticalDrivesNames = opticalDrives.split(QRegExp("\\s+"));

        this->dOptical.clear();
        for (int counter=0; counter < opticalDrivesNames.size(); ++counter)
        {
            opticalDrives = QString("/dev/%1").arg(opticalDrivesNames.at(counter));
            qDebug() << opticalDrives;

            this->dOptical.append(opticalDrives);
        }
    }
    else
    {
        //this->dOptical.append(tr("No optical drives found"));
        qDebug() << "No optical drives found; list length:" << dOptical.length();
    }

    qDebug() << "dOptical:" << this->dOptical;
}



/*
 * Get network interfaces and addresses
 *
 */
void MachineInfoTable::getNetworkInterfaces()
{
    this->dNetworkInterfaces.clear();

    QString networkInterfaceString;

    foreach (QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        if (netInterface.name() != "lo") // Ignore local loop interface
        {
            networkInterfaceString = QString(tr("%1  (MAC %2)")).arg(netInterface.name())
                                                                .arg(netInterface.hardwareAddress());

            foreach (QNetworkAddressEntry address, netInterface.addressEntries())
            {
                networkInterfaceString.append("\n > ");
                networkInterfaceString.append(address.ip().toString());
                networkInterfaceString.append(" / ");
                networkInterfaceString.append(address.netmask().toString());
            }

            dNetworkInterfaces.append(networkInterfaceString);
        }
    }

    qDebug() << "dNetworkInterfaces:" << this->dNetworkInterfaces;
}




/*
 * Get resolution information
 *
 */
void MachineInfoTable::getResolution()
{
    qDebug() << "getting resolution info";
    QDesktopWidget desktop;
    QString resolution("");
    resolution.append(QString("%1 x %2 pixels, %3 bpp")
                      .arg(desktop.width())
                      .arg(desktop.height())
                      .arg(desktop.depth()));

    this->dResolution = resolution;
}




/*
 * Get kernel version information
 *
 */
void MachineInfoTable::getKernel()
{
    qDebug() << "getting kernel info";
    QFile kernelInfoFile("/proc/version");
    kernelInfoFile.open(QIODevice::ReadOnly);

    QString kernel("");
    kernel.append(kernelInfoFile.readAll().trimmed());
    kernelInfoFile.close();

    this->dKernel = kernel;
}


void MachineInfoTable::getUptime()
{
    qDebug() << "getting uptime info";
    QFile uptimeInfoFile("/proc/uptime"); // system uptime in seconds
    uptimeInfoFile.open(QIODevice::ReadOnly);

    QString uptime("");
    uptime.append(uptimeInfoFile.readAll().trimmed());
    uptimeInfoFile.close();
    uptime = uptime.split(" ").at(0);

    float rawUptime; // seconds
    rawUptime = uptime.toFloat(); // TMPFIX, verify with "bool *ok" parameter


    int days    = 0;
    int hours   = 0;
    int minutes = 0;
    int seconds = 0;

    while (rawUptime >= 60)
    {
        ++seconds;
        if (seconds == 60)
        {
            seconds = 0;
            ++minutes;
            if (minutes == 60)
            {
                minutes = 0;
                ++hours;
                if (hours == 24)
                {
                    hours = 0;
                    ++days;
                }
            }
        }

        --rawUptime;
    }
    seconds = rawUptime;


    uptime = tr("%1 days, %2 hours, %3 minutes, %4 seconds")
             .arg(days).arg(hours).arg(minutes).arg(seconds);

    this->dUptime = uptime;
}



/*
 *  Getters for stored values detected on MachineInfoTable::get*()
 */
QString MachineInfoTable::cpu()
{
    return this->dCPU;
}
QString MachineInfoTable::memory()
{
    return QString ("%1 (%2 free)").arg(this->dTotalMemory).arg(this->dFreeMemory);
}
QStringList MachineInfoTable::disks()
{
    return this->dDisks;
}
QStringList MachineInfoTable::disksDev()
{
    return this->dDisksDev;
}
QStringList MachineInfoTable::optical()
{
    return this->dOptical;
}
QStringList MachineInfoTable::networkInterfaces()
{
    return this->dNetworkInterfaces;
}

QString MachineInfoTable::resolution()
{
    return this->dResolution;
}
QString MachineInfoTable::kernel()
{
    return this->dKernel;
}
QString MachineInfoTable::uptime()
{
    return this->dUptime;
}
QString MachineInfoTable::sensorsInfoText()
{
    return this->sensorsManager->sensorsFullInfo();
}
