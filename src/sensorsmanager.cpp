/*
 *   This file is part of StressItOut
 *   Copyright 2010-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "sensorsmanager.h"

SensorsManager::SensorsManager(QObject *parent) : QObject(parent)
{
    this->sensorsInfoString = tr("You should see sensor values here.");

    sensorsAvailable = sensors_init(NULL);
    qDebug() << "sensors_init():" << sensorsAvailable << " (0 means OK)";
    if (sensorsAvailable == 0) // If libsensors init OK
    {
        qDebug() << "libsensors version:" << libsensors_version;

        this->getAllSensorsInfo();
    }



    qDebug() << "SensorsManager created";
}


SensorsManager::~SensorsManager()
{
    qDebug() << "SensorsManager destroyed";
}


void SensorsManager::getAllSensorsInfo()
{
    if (sensorsAvailable == 0) // If libsensors init OK
    {
        sensorsInfoString.clear();
        qDebug() << "Listing sensor chips:";

        // tmp sensors stuff / tests
        const sensors_chip_name *chipName;
        const sensors_feature *chipFeature;
        const sensors_subfeature *chipSubfeature;

        int chipNumber = 0;

        while ((chipName = sensors_get_detected_chips(NULL, &chipNumber)))
        {
            qDebug() << "Chip " << chipNumber;

            qDebug() << "addr: " << chipName->addr;
            qDebug() << "path: " << chipName->path;
            qDebug() << "prefix: " << chipName->prefix;

            sensorsInfoString.append(tr("Chip %1").arg(chipNumber) + "\n");
            sensorsInfoString.append(tr("Address: %1").arg(chipName->addr) + "\n");
            sensorsInfoString.append(tr("Path: %1").arg(chipName->path) + "\n");
            sensorsInfoString.append(tr("Prefix: %1").arg(chipName->prefix) + "\n");


            qDebug() << " :: Features ::";
            sensorsInfoString.append("\n" + tr("Features:") + "\n");


            // FEATURE LIST
            // double loop as per chips.c

            double sensorValue;
            int err;

            int featureNumber;
            int subFeatureNumber;


            featureNumber = 0;
            while ((chipFeature = sensors_get_features(chipName, &featureNumber)))
            {
                qDebug() << QString("Feature: %1").arg(chipFeature->name);
                sensorsInfoString.append(tr("%1:").arg(chipFeature->name) + " ");

                subFeatureNumber = 0;
                while ((chipSubfeature = sensors_get_all_subfeatures(chipName, chipFeature, &subFeatureNumber)))
                {
                    err = sensors_get_value(chipName, chipSubfeature->number, &sensorValue);
                    if (err == 0)
                    {
                        qDebug() << "value: " << sensorValue;
                        sensorsInfoString.append(QString("%1 ").arg(sensorValue));

                        switch (chipFeature->type)
                        {
                        case SENSORS_FEATURE_TEMP: // temperature, degrees Celsius symbol
                            sensorsInfoString.append(QString::fromUtf8("\342\204\203"));
                            break;

                        case SENSORS_FEATURE_IN:  // input voltages
                            sensorsInfoString.append("V");
                            break;

                        case SENSORS_FEATURE_FAN: // Fan's RPM
                            sensorsInfoString.append("RPM");
                            break;

                        default:
                            break; // This is to avoid a compiler warning
                        }


                        sensorsInfoString.append("  |  ");
                    }
                    else
                    {
                        qDebug() << "Error: " << err;
                    }
                }
                sensorsInfoString.append("\n");
            }
        }
        // end sensors tests
    }
}



///////////////////////////////// SLOTS /////////////////////////////////////////



QString SensorsManager::sensorsFullInfo()
{
    if (sensorsInfoString.isEmpty())
    {
        return tr("No sensors detected, or not properly configured.\n"
                  "See lm_sensors documentation for more information.\n\n"
                  "You'll probably need to run sensors-detect.");
    }
    else
    {
        getAllSensorsInfo();
        return this->sensorsInfoString;
    }
}
